FROM registry.sme.uni-bamberg.de/bambirds/testing/build as build

WORKDIR /opt/bambirds-lite

COPY . /opt/bambirds-lite/

RUN ./gradlew installDist

FROM registry.sme.uni-bamberg.de/bambirds/testing/base

COPY --from=build /opt/bambirds-lite/build/install/bambirds-lite /opt/bambirds-lite

ENTRYPOINT [ "/opt/bambirds-lite/bin/bambirds-lite" ]

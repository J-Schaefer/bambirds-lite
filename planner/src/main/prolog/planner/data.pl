:- module(data, [
%% from data file
% belongsTo(Object, Structure)
	belongsTo/2,
% isAnchorPointFor(LeftmostObject, Structure)
	isAnchorPointFor/2,
% isCollapsable(Structure)
	isCollapsable/1,
% isBelow(ObjectA, ObjectB)
	isBelow/2,
% isLeft(ObjectA, ObjectB)
	isLeft/2,
% isOn(ObjectA, ObjectB)
	isOn/2,
% isOver(ObjectA, ObjectB)
	isOver/2,
% isRight(ObjectA, ObjectB)
	isRight/2,
% isTower(Structure)
	isTower/1,
% bird(Bird)
	bird/1,
% birdOrder(Bird, Index)
	birdOrder/2,
% canCollapse(StructA, StructB)
	canCollapse/2,
% canExplode(TnT, Object)
	canExplode/2,
% collapsesInDirection(StructA, StructB, towards|away)
	collapsesInDirection/3,
% ground_plane(Y)
	ground_plane/1,
% hasColor(Object, Color)
	hasColor/2,
% hasForm(Object, ball|cube|bar|block)
	hasForm/2,
% hasOrientation(Object, horizontal|vertical)
	hasOrientation/2,
% hasSize(Object, small|medium|big)
	hasSize/2,
% hill(Object, X, Y, Width, Height)
	hill/5,

% pig(ObjectA, X, Y, Width, Height)
	pig/5,
% protects(ObjectA, ObjectB)
	protects/2,
% scene_scale(Scale, ScalingFactor)
	scene_scale/2,
% shape(Object, rect|ball|poly|unknown, CenterX, CenterY, Size, Props)
% Props format depends on the shape of the object:
% - rect -> [Width, Height, Angle]
% - ball -> [Radius]
% - poly -> [NumPoints, [Point1X,Point1Y], ..., [PointNX, PointNY]]
% - unknown -> []
	shape/6,
% situation_name(Filename)
	situation_name/1,
% slingshotPivot(X, Y)
	slingshotPivot/2,
% structure(Structure)
	structure/1,
% hasMaterial(Object, pork|ice|wood|stone|tnt, X, Y, Width, Height)
	hasMaterial/6,

%"% generated"
% isHittable(Object, Angle)
	isHittable/2,
% parabola(Object, [HitX, HitY], ImpactAngle, A, B)
	parabola/5,
% current_filename(Filename)
	current_filename/1,

%% dynamic predicates
% object(Object)
% Objects can be hills, pigs or blocks
	object/1,

%% functions
% purge() -> deletes all data
	purge/0,
% load_data(Filename) -> loads all data from Filename
	load_data/1
]).

:- dynamic(isHittable/2).
:- dynamic(isAnchorPointFor/2).
:- dynamic(isCollapsable/1).
:- dynamic(isBelow/2).
:- dynamic(isLeft/2).
:- dynamic(isOn/2).
:- dynamic(isOver/2).
:- dynamic(isRight/2).
:- dynamic(isTower/1).
:- dynamic(belongsTo/2).
:- dynamic(bird/1).
:- dynamic(birdOrder/2).
:- dynamic(canCollapse/2).
:- dynamic(canExplode/2).
:- dynamic(collapsesInDirection/3).
:- dynamic(ground_plane/1).
:- dynamic(hasColor/2).
:- dynamic(hasForm/2).
:- dynamic(hasOrientation/2).
:- dynamic(hasSize/2).
:- dynamic(hill/5).
:- dynamic(parabola/5).
:- dynamic(pig/5).
:- dynamic(protects/2).
:- dynamic(scene_scale/2).
:- dynamic(shape/6).
:- dynamic(situation_name/1).
:- dynamic(slingshotPivot/2).
:- dynamic(structure/1).
:- dynamic(supports/2).
:- dynamic(hasMaterial/6).
:- dynamic(current_filename/1).
:- dynamic(sceneRepresentation/1).
object(X) :- hasMaterial(X,_,_,_,_,_).
object(X) :- hill(X,_,_,_,_).
object(X) :- pig(X,_,_,_,_).

purge :-
	retractall(isHittable(_,_)),
	retractall(isAnchorPointFor(_,_)),
	retractall(isCollapsable(_)),
	retractall(isBelow(_,_)),
	retractall(isLeft(_,_)),
	retractall(isOn(_,_)),
	retractall(isOver(_,_)),
	retractall(isRight(_,_)),
	retractall(isTower(_)),
	retractall(belongsTo(_,_)),
	retractall(bird(_)),
	retractall(birdOrder(_,_)),
	retractall(canCollapse(_,_)),
	retractall(canExplode(_,_)),
	retractall(collapsesInDirection(_,_,_)),
	retractall(ground_plane(_)),
	retractall(hasColor(_,_)),
	retractall(hasForm(_,_)),
	retractall(hasOrientation(_,_)),
	retractall(hasSize(_,_)),
	retractall(hill(_,_,_,_,_)),
	retractall(parabola(_,_,_,_,_)),
	retractall(pig(_,_,_,_,_)),
	retractall(protects(_,_)),
	retractall(scene_scale(_,_)),
	retractall(shape(_,_,_,_,_,_)),
	retractall(situation_name(_)),
	retractall(sceneRepresentation(_)),
	retractall(slingshotPivot(_,_)),
	retractall(structure(_)),
	retractall(supports(_,_)),
	retractall(hasMaterial(_,_,_,_,_,_)).

load_data(Filename) :-
	((
			current_filename(CurrentFilename),
			unload_file(CurrentFilename),
			retractall(current_filename(_))
		) ->
		true;true),
	purge,
	consult(Filename),
	asserta(current_filename(Filename)).
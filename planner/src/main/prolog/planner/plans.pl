:- module(plans, [
    simple_plans/1,
    plan/2
]).  % export planner entry point and basic plan predicate

:- use_module(shot/hittable).
:- use_module(shot/simple).
:- use_module(contrib/between_with_step).
:- use_module(ab).
:- use_module(data).
:- use_module(common/list).
:- use_module(tikz).
:- use_module(library(http/json)).
:- use_module(physics/impact).


%% utility for printing out a plan once found (so it does not get lost if planner runs into timeout!)
print_intermediate_plan(Plan) :-
	write('%'),                                   % marks plan as 'intermediate result'
	json_write(current_output, Plan, [width(0)]),
	writeln(""),
	flush_output.

print_text(Text) :-
	write('%'),                                   % marks plan as 'intermediate result'
	json_write(current_output, Text, [width(0)]),
	writeln(""),
	flush_output.

%% predicate for comparing plans, two criteria:
%%   primary: plans with higher confidence go first
%%   secondary: plans that promise to kill more pigs are preferred
plan_compare(=, Plan, Plan).
plan_compare(REL, Plan1, Plan2) :-
	length(Plan1.reasons,Reasons1Length),
	length(Plan2.reasons,Reasons2Length),
	((Plan1.confidence + 0.1*Reasons1Length =< Plan1.confidence + 0.1*Reasons2Length) -> REL = > ; REL = <).


%% our (simple) planner: collect all plans from plan/2 and (for convenience) sort them according to agents preference
% allPlans_(-ListOfPlans)
simple_plans(SortedPlans) :-
    in_slingshot(Bird),                              % plan for bird to be fired next
    (setof(Plan,                                     % collect all plans...
	   (
	       plan(Bird, Plan),
	       print_intermediate_plan(Plan)
	   ),
	   AllPlans)
    ;
    AllPlans=[]),                                    % or set AllPlans to empty list if planning fails
    predsort(plan_compare, AllPlans, SortedPlans),   % sort plans to remove duplicates (if any)
    write_tikz(SortedPlans).                         % write images for debugging


%%
%% ---------// ACTUAL PLANS COMPUTED HERE //--------
%%

%% example of two simple plans:
%% 1.: shoot at any pig that is hittable, i.e., not covered by some obstacle
%% (in the real game, often most pigs are covered -- you need better plans)
%%
%% 2.: shoot at any pig hidden behind a single obstacle
%% (not really sensible, but shows how to retrieve obstacles and shoot at X/Y coordinates)
%%
%% add as many plans you like using the same format as in the example
%%

%% arguments of plan/2:
%%
%% Bird    : the bird for which the shot is to be planned
%% Plan    : plan found; plan is a record with the following elements:
%%            bird: the bird for which the plan is valid (same bird as in first argument)
%%            shot: a record that contains all information for delivering the shot (dont worry about this one)
%%            target_object (optional) : the object (wood2, ice1, stone4, ...) aimed at
%%            impact_angle  (optional) : angle at which bird hits target (allows us to differentiate different shots at a target)
%%            strategy                 : name of the strategy that suggests this plan
%%            confidence (important)   : 0.0...1.0 real number giving confidence that this plan is smart (1.0 -> 100% sure)
%%            reasons  (optional)      : unused in BamBirds Lite except for debug; list Pigs this plan achieves to destroy

num_pigs(Count) :-
    aggregate_all(count, pig(P), Count).

num_birds(Count) :-
    aggregate_all(count, bird(B), Count).

% Finds a plan for targetting a pig, returns a list of a target, the strategy chosen, and confidence
% plan_(-DecisionList)
plan(Bird, plan{bird:Bird, shot:Shot, target_object:Target, impact_angle:ImpactAngle, strategy:"targetPigDirectly", confidence:C, reasons:[Target]}) :-
    num_pigs(NP),
    num_birds(NB),
    NB >= NP,
    pig(Target),                                    % unify Target with some object of type pig
    isHittable(Target, ImpactAngle),                % ensure Target is directly hittable (and perform backtracking to pig(Target), if not)
    C is 0.8,                                       % set confidence to 80% that shot is sensible
    shot_at_target(Target,ImpactAngle, Shot).       % compute shot parameters (needed by agent to launch bird)


plan(Bird, plan{bird:Bird, shot:Shot, target_object:Target, impact_angle:ImpactAngle, strategy:"targetExplosivePig", confidence:C, reasons:[Target]}) :-
    canExplode(Target, Pig),
    pig(Pig),
    isHittable(Target, ImpactAngle),
    C is 0.9,
    shot_at_target(Target,ImpactAngle, Shot).


plan(Bird, plan{bird:Bird, shot:Shot, target_object:Target, impact_angle:ImpactAngle, strategy:"targetExplosive", confidence:C, reasons:[Target]}) :-
    canExplode(Target,Stone),
    hasMaterial(Stone,stone,_,_,_,_),
    isHittable(Target, ImpactAngle),
    C is 0.7,
    shot_at_target(Target,ImpactAngle, Shot).


plan(Bird, plan{bird:Bird, shot:Shot, target_object:Target, impact_angle:ImpactAngle, strategy:"targetAnythingExplosive", confidence:C, reasons:[Target]}) :-
    canExplode(Target,_),
    isHittable(Target, ImpactAngle),
    C is 0.7,
    shot_params_dict(ImpactAngle, Shot).


plan(Bird, plan{bird:Bird, shot:Shot, target_object:Target, impact_angle:ImpactAngle, strategy:"targetExplosiveStructure", confidence:C, reasons:[Target]}) :-
    canExplode(Target, Object),
    belongsTo(Object, Structure),
    % belongsTo(Object2, Structure),
    % supports(Object, Object2),
    % Object \= Object2,
    protects(Structure, Pig),
    pig(Pig),
    isHittable(Target, ImpactAngle),
    C is 0.6,
    shot_at_target(Target,ImpactAngle, Shot).


plan(Bird, plan{bird:Bird, shot:Shot, target_object:SupportObj, impact_angle:ImpactAngle, strategy:"targetStructure", confidence:C, reasons:[Target]}) :-
    pig(Target),
    protects(Structure, Target),
    belongsTo(SupportObj, Structure),
    isOn(_,SupportObj),
    isHittable(SupportObj, ImpactAngle),
    impact(Bird, SupportObj, ImpactScore),
    ImpactScore > 1,
    C is 0.6,
    shot_at_target(SupportObj, ImpactAngle, Shot).


plan(Bird, plan{bird:Bird, shot:Shot, target_object:Target, impact_angle:ImpactAngle, strategy:"targetBall", confidence:C, reasons:[Target]}) :-
    pig(Target),
    shape(Ball,ball,XB,YB,_,_),
    \+pig(Ball),
    \+bird(Ball),
    shape(Target,_,XP,YP,_,_),
    XB<XP,
    YB<YP,
    isHittable(Ball, ImpactAngle),
    C is 0.6,
    shot_params_dict(ImpactAngle, Shot).


% Finds a plan for targetting a pig hidden behind a single obstacle
plan(Bird, plan{bird:Bird, shot:Shot, target_object:Target, impact_angle:ImpactAngle, strategy:"targetPigBehindObstacle", confidence:C, reasons:[Target]}) :-
    pig(Target),                                                   % unify Target with some object of type pig
    shape(Target,_,TX,TY,_,_),                                     % retrieve shape information: TX,TY represents center of target
    shots_at_point(Bird, Target, TX, TY, ImpactAngleAndObstacles), % ImpactAngleAndObstacles is list of shots (max. 2) [ImpactAngle, ListOfObstacles]
    member([ImpactAngle,[[Obstacle,_,_]]], ImpactAngleAndObstacles),                     % [_] only matches to list with single element
    \+hill(Obstacle),
    C is 0.5,                                                      % low confidence
    shot_at_point(TX,TY,ImpactAngle,Shot).                                  % compute shot


:- module(shot, []).

:- reexport([
	shot/collision_shapes,
	shot/simple,
	shot/hittable,
	shot/obstacles
	]).
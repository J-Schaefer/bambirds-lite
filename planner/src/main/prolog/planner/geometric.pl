:- module(geometric, []).

:- reexport([
	geometric/angles,
	geometric/boxes,
	geometric/common,
	geometric/intersection,
	geometric/polygon]).
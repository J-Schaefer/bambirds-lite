belongsTo(pig0,struct1).
belongsTo(wood0,struct0).
bird(redbird0).
birdOrder(redbird0,0).
collapsesInDirection(struct0,struct1,away).
collapsesInDirection(struct1,struct0,towards).
ground_plane(384).
hasColor(redbird0,red).
hasForm(wood0,block).
hasMaterial(pig0,pork,437,318,7,7).
hasMaterial(wood0,wood,376,275,20,19).
hasOrientation(wood0,horizontal).
hasSize(hill0,medium).
hasSize(pig0,medium).
hasSize(redbird0,medium).
hasSize(wood0,medium).
hill(hill0,279,227,192,136).
isAnchorPointFor(pig0,struct1).
isAnchorPointFor(wood0,struct0).
isCollapsable(struct0).
isCollapsable(struct1).
isOn(pig0,hill0).
isOn(wood0,hill0).
isOver(hill0,pig0).
pig(pig0,437,318,7,7).
scene_scale(55,1.005).
shape(hill0,poly, 447,296,26112,[15,[279,362],[280,363],[471,360],[470,227],[377,228],[378,269],[407,269],[447,309],[446,326],[433,326],[400,293],[377,294],[376,361],[347,362],[324,360]]).
shape(pig0,ball, 440,321.5,70,[4.75]).
shape(redbird0,ball, 191.5,318.5,38,[3.5]).
shape(wood0,ball, 386,284.5,201,[14]).
situation_name('Level1-10').
slingshotPivot(191.28,322.02).
structure(struct0).
structure(struct1).

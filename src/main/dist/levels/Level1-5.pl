belongsTo(ice0,struct0).
belongsTo(ice1,struct0).
belongsTo(ice2,struct1).
belongsTo(ice3,struct2).
belongsTo(ice4,struct2).
belongsTo(pig0,struct0).
belongsTo(pig1,struct1).
belongsTo(pig2,struct2).
belongsTo(pig3,struct3).
belongsTo(pig4,struct4).
belongsTo(pig5,struct5).
belongsTo(pig6,struct6).
belongsTo(pig7,struct7).
belongsTo(pig8,struct8).
belongsTo(stone10,struct6).
belongsTo(stone11,struct6).
belongsTo(stone12,struct7).
belongsTo(stone13,struct7).
belongsTo(stone14,struct8).
belongsTo(stone15,struct8).
belongsTo(wood5,struct3).
belongsTo(wood6,struct3).
belongsTo(wood7,struct4).
belongsTo(wood8,struct4).
belongsTo(wood9,struct5).
bird(redbird0).
bird(redbird1).
bird(redbird2).
bird(redbird3).
birdOrder(redbird0,0).
birdOrder(redbird1,1).
birdOrder(redbird2,2).
birdOrder(redbird3,3).
canCollapse(struct0,struct1).
canCollapse(struct1,struct0).
canCollapse(struct1,struct2).
canCollapse(struct2,struct1).
canCollapse(struct2,struct3).
canCollapse(struct3,struct2).
canCollapse(struct3,struct4).
canCollapse(struct4,struct3).
canCollapse(struct4,struct5).
canCollapse(struct5,struct4).
canCollapse(struct5,struct6).
canCollapse(struct6,struct5).
canCollapse(struct6,struct7).
canCollapse(struct7,struct6).
canCollapse(struct7,struct8).
canCollapse(struct8,struct7).
collapsesInDirection(struct0,struct1,away).
collapsesInDirection(struct1,struct0,towards).
collapsesInDirection(struct1,struct2,away).
collapsesInDirection(struct2,struct1,towards).
collapsesInDirection(struct2,struct3,away).
collapsesInDirection(struct3,struct2,towards).
collapsesInDirection(struct3,struct4,away).
collapsesInDirection(struct4,struct3,towards).
collapsesInDirection(struct4,struct5,away).
collapsesInDirection(struct5,struct4,towards).
collapsesInDirection(struct5,struct6,away).
collapsesInDirection(struct6,struct5,towards).
collapsesInDirection(struct6,struct7,away).
collapsesInDirection(struct7,struct6,towards).
collapsesInDirection(struct7,struct8,away).
collapsesInDirection(struct8,struct7,towards).
ground_plane(384).
hasColor(redbird0,red).
hasColor(redbird1,red).
hasColor(redbird2,red).
hasColor(redbird3,red).
hasForm(ice0,bar).
hasForm(ice1,bar).
hasForm(ice2,bar).
hasForm(ice3,bar).
hasForm(ice4,bar).
hasForm(stone10,bar).
hasForm(stone11,bar).
hasForm(stone12,bar).
hasForm(stone13,bar).
hasForm(stone14,bar).
hasForm(stone15,bar).
hasForm(wood5,bar).
hasForm(wood6,bar).
hasForm(wood7,bar).
hasForm(wood8,bar).
hasForm(wood9,bar).
hasMaterial(ice0,ice,298,334,17,5).
hasMaterial(ice1,ice,304,338,4,18).
hasMaterial(ice2,ice,316,316,17,5).
hasMaterial(ice3,ice,334,308,17,4).
hasMaterial(ice4,ice,341,314,4,43).
hasMaterial(pig0,pork,303,326,7,7).
hasMaterial(pig1,pork,322,308,6,6).
hasMaterial(pig2,pork,340,300,6,6).
hasMaterial(pig3,pork,358,289,7,7).
hasMaterial(pig4,pork,377,272,6,6).
hasMaterial(pig5,pork,395,264,6,6).
hasMaterial(pig6,pork,413,254,6,6).
hasMaterial(pig7,pork,431,235,7,7).
hasMaterial(pig8,pork,450,228,7,7).
hasMaterial(stone10,stone,408,262,17,4).
hasMaterial(stone11,stone,415,267,4,18).
hasMaterial(stone12,stone,426,244,17,5).
hasMaterial(stone13,stone,433,249,4,36).
hasMaterial(stone14,stone,444,236,18,4).
hasMaterial(stone15,stone,451,241,4,44).
hasMaterial(wood5,wood,354,298,17,4).
hasMaterial(wood6,wood,360,303,4,17).
hasMaterial(wood7,wood,372,280,17,4).
hasMaterial(wood8,wood,378,285,4,35).
hasMaterial(wood9,wood,390,272,17,4).
hasOrientation(ice0,horizontal).
hasOrientation(ice1,vertical).
hasOrientation(ice2,horizontal).
hasOrientation(ice3,horizontal).
hasOrientation(ice4,vertical).
hasOrientation(stone10,horizontal).
hasOrientation(stone11,vertical).
hasOrientation(stone12,horizontal).
hasOrientation(stone13,vertical).
hasOrientation(stone14,horizontal).
hasOrientation(stone15,vertical).
hasOrientation(wood5,horizontal).
hasOrientation(wood6,vertical).
hasOrientation(wood7,horizontal).
hasOrientation(wood8,vertical).
hasOrientation(wood9,horizontal).
hasSize(hill0,big).
hasSize(hill1,big).
hasSize(ice0,small).
hasSize(ice1,medium).
hasSize(ice2,small).
hasSize(ice3,small).
hasSize(ice4,big).
hasSize(pig0,medium).
hasSize(pig1,medium).
hasSize(pig2,medium).
hasSize(pig3,medium).
hasSize(pig4,medium).
hasSize(pig5,medium).
hasSize(pig6,medium).
hasSize(pig7,medium).
hasSize(pig8,medium).
hasSize(redbird0,medium).
hasSize(redbird1,medium).
hasSize(redbird2,medium).
hasSize(redbird3,medium).
hasSize(stone10,small).
hasSize(stone11,medium).
hasSize(stone12,small).
hasSize(stone13,big).
hasSize(stone14,medium).
hasSize(stone15,big).
hasSize(wood5,small).
hasSize(wood6,small).
hasSize(wood7,small).
hasSize(wood8,big).
hasSize(wood9,small).
hill(hill0,0,350,163,10).
hill(hill1,283,195,356,164).
isAnchorPointFor(ice0,struct0).
isAnchorPointFor(ice2,struct1).
isAnchorPointFor(ice3,struct2).
isAnchorPointFor(stone10,struct6).
isAnchorPointFor(stone12,struct7).
isAnchorPointFor(stone14,struct8).
isAnchorPointFor(wood5,struct3).
isAnchorPointFor(wood7,struct4).
isAnchorPointFor(wood9,struct5).
isBelow(ice0,pig0).
isBelow(ice1,ice0).
isBelow(ice2,pig1).
isBelow(ice3,pig2).
isBelow(ice4,ice3).
isBelow(stone10,pig6).
isBelow(stone11,stone10).
isBelow(stone12,pig7).
isBelow(stone13,stone12).
isBelow(stone14,pig8).
isBelow(stone15,stone14).
isBelow(wood5,pig3).
isBelow(wood6,wood5).
isBelow(wood7,pig4).
isBelow(wood8,wood7).
isBelow(wood9,pig5).
isCollapsable(struct0).
isCollapsable(struct1).
isCollapsable(struct2).
isCollapsable(struct3).
isCollapsable(struct4).
isCollapsable(struct5).
isCollapsable(struct6).
isCollapsable(struct7).
isCollapsable(struct8).
isOn(ice0,hill1).
isOn(ice0,ice1).
isOn(ice1,hill1).
isOn(ice2,hill1).
isOn(ice3,hill1).
isOn(ice3,ice4).
isOn(ice4,hill1).
isOn(pig0,hill1).
isOn(pig0,ice0).
isOn(pig1,hill1).
isOn(pig1,ice2).
isOn(pig2,hill1).
isOn(pig2,ice3).
isOn(pig3,hill1).
isOn(pig3,wood5).
isOn(pig4,hill1).
isOn(pig4,wood7).
isOn(pig5,hill1).
isOn(pig5,wood9).
isOn(pig6,hill1).
isOn(pig6,stone10).
isOn(pig7,hill1).
isOn(pig7,stone12).
isOn(pig8,hill1).
isOn(pig8,stone14).
isOn(stone10,hill1).
isOn(stone10,stone11).
isOn(stone11,hill1).
isOn(stone12,hill1).
isOn(stone12,stone13).
isOn(stone13,hill1).
isOn(stone14,hill1).
isOn(stone14,stone15).
isOn(stone15,hill1).
isOn(wood5,hill1).
isOn(wood5,wood6).
isOn(wood6,hill1).
isOn(wood7,hill1).
isOn(wood7,wood8).
isOn(wood8,hill1).
isOn(wood9,hill1).
isOver(hill1,pig0).
isOver(hill1,pig1).
isOver(hill1,pig2).
isOver(hill1,pig3).
isOver(hill1,pig4).
isOver(hill1,pig5).
isOver(hill1,pig6).
isOver(hill1,pig7).
isOver(hill1,pig8).
isTower(struct0).
isTower(struct2).
isTower(struct3).
isTower(struct4).
isTower(struct6).
isTower(struct7).
isTower(struct8).
pig(pig0,303,326,7,7).
pig(pig1,322,308,6,6).
pig(pig2,340,300,6,6).
pig(pig3,358,289,7,7).
pig(pig4,377,272,6,6).
pig(pig5,395,264,6,6).
pig(pig6,413,254,6,6).
pig(pig7,431,235,7,7).
pig(pig8,450,228,7,7).
scene_scale(53,1.005).
shape(hill0,poly, 141,355,1630,[6,[0,357],[0,360],[163,356],[161,350],[118,351],[117,356]]).
shape(hill1,poly, 461,277,58384,[14,[283,358],[284,359],[639,358],[639,356],[556,356],[555,285],[576,205],[535,195],[519,254],[488,285],[408,286],[408,321],[359,322],[359,358]]).
shape(ice0,rect, 306.5,336.5,85,[5,17,0]).
shape(ice1,rect, 306,347,72,[4,18,1.57079633]).
shape(ice2,rect, 324.5,318.5,85,[5,17,0]).
shape(ice3,rect, 342.5,310,68,[4,17,0]).
shape(ice4,rect, 343,335.5,172,[4,43,1.57079633]).
shape(pig0,ball, 306.5,329,70,[4.75]).
shape(pig1,ball, 325,311,63,[4.5]).
shape(pig2,ball, 343.5,303.5,63,[4.5]).
shape(pig3,ball, 362,293,78,[5]).
shape(pig4,ball, 380.5,275.5,63,[4.5]).
shape(pig5,ball, 398.5,267.5,63,[4.5]).
shape(pig6,ball, 416.5,257.5,63,[4.5]).
shape(pig7,ball, 435,239,78,[5]).
shape(pig8,ball, 453,231.5,70,[4.75]).
shape(redbird0,ball, 125.5,310.5,38,[3.5]).
shape(redbird1,ball, 109,351.5,44,[3.75]).
shape(redbird2,ball, 92.5,351,56,[4.25]).
shape(redbird3,ball, 75.5,351,56,[4.25]).
shape(stone10,rect, 416.5,264,68,[4,17,0]).
shape(stone11,rect, 417,276,72,[4,18,1.57079633]).
shape(stone12,rect, 434.5,246.5,85,[5,17,0]).
shape(stone13,rect, 435,267,144,[4,36,1.57079633]).
shape(stone14,rect, 453,238,72,[4,18,0]).
shape(stone15,rect, 453,263,176,[4,44,1.57079633]).
shape(wood5,rect, 362.5,300,68,[4,17,0]).
shape(wood6,rect, 362,311.5,68,[4,17,1.57079633]).
shape(wood7,rect, 380.5,282,68,[4,17,0]).
shape(wood8,rect, 380,302.5,140,[4,35,1.57079633]).
shape(wood9,rect, 398.5,274,68,[4,17,0]).
situation_name('Level1-5').
slingshotPivot(125.28,314.02).
structure(struct0).
structure(struct1).
structure(struct2).
structure(struct3).
structure(struct4).
structure(struct5).
structure(struct6).
structure(struct7).
structure(struct8).

belongsTo(ice5,struct3).
belongsTo(ice6,struct3).
belongsTo(ice7,struct3).
belongsTo(ice8,struct3).
belongsTo(ice9,struct3).
belongsTo(pig0,struct0).
belongsTo(pig1,struct2).
belongsTo(wood0,struct1).
belongsTo(wood1,struct1).
belongsTo(wood2,struct1).
belongsTo(wood3,struct1).
belongsTo(wood4,struct1).
bird(bluebird0).
birdOrder(bluebird0,0).
canCollapse(struct0,struct1).
canCollapse(struct1,struct0).
canCollapse(struct2,struct1).
canCollapse(struct2,struct3).
canCollapse(struct3,struct2).
collapsesInDirection(struct0,struct1,away).
collapsesInDirection(struct1,struct0,towards).
collapsesInDirection(struct1,struct2,away).
collapsesInDirection(struct2,struct1,towards).
collapsesInDirection(struct2,struct3,away).
collapsesInDirection(struct3,struct2,towards).
ground_plane(384).
hasColor(bluebird0,blue).
hasForm(ice5,block).
hasForm(ice6,bar).
hasForm(ice7,bar).
hasForm(ice8,bar).
hasForm(ice9,block).
hasForm(wood0,block).
hasForm(wood1,bar).
hasForm(wood2,bar).
hasForm(wood3,bar).
hasForm(wood4,block).
hasMaterial(ice5,ice,363,255,23,24).
hasMaterial(ice6,ice,370,249,58,6).
hasMaterial(ice7,ice,375,241,48,7).
hasMaterial(ice8,ice,387,235,23,6).
hasMaterial(ice9,ice,413,255,22,23).
hasMaterial(pig0,pork,270,389,9,9).
hasMaterial(pig1,pork,397,269,9,9).
hasMaterial(wood0,wood,236,375,23,24).
hasMaterial(wood1,wood,243,368,58,6).
hasMaterial(wood2,wood,248,361,48,6).
hasMaterial(wood3,wood,260,355,23,5).
hasMaterial(wood4,wood,285,375,23,24).
hasOrientation(ice5,vertical).
hasOrientation(ice6,horizontal).
hasOrientation(ice7,horizontal).
hasOrientation(ice8,horizontal).
hasOrientation(ice9,vertical).
hasOrientation(wood0,vertical).
hasOrientation(wood1,horizontal).
hasOrientation(wood2,horizontal).
hasOrientation(wood3,horizontal).
hasOrientation(wood4,vertical).
hasSize(bluebird0,medium).
hasSize(hill0,small).
hasSize(hill1,big).
hasSize(hill2,big).
hasSize(ice5,medium).
hasSize(ice6,big).
hasSize(ice7,big).
hasSize(ice8,big).
hasSize(ice9,medium).
hasSize(pig0,medium).
hasSize(pig1,medium).
hasSize(wood0,medium).
hasSize(wood1,big).
hasSize(wood2,big).
hasSize(wood3,big).
hasSize(wood4,medium).
hill(hill0,37,401,0,1).
hill(hill1,332,400,307,2).
hill(hill2,355,279,86,31).
isAnchorPointFor(ice5,struct3).
isAnchorPointFor(pig0,struct0).
isAnchorPointFor(pig1,struct2).
isAnchorPointFor(wood0,struct1).
isBelow(ice5,ice6).
isBelow(ice6,ice7).
isBelow(ice7,ice8).
isBelow(ice9,ice6).
isBelow(wood0,wood1).
isBelow(wood1,wood2).
isBelow(wood2,wood3).
isBelow(wood4,wood1).
isCollapsable(struct0).
isCollapsable(struct1).
isCollapsable(struct2).
isCollapsable(struct3).
isOn(ice5,hill2).
isOn(ice6,ice5).
isOn(ice6,ice9).
isOn(ice7,ice6).
isOn(ice8,ice7).
isOn(ice9,hill2).
isOn(pig0,ground).
isOn(pig1,hill2).
isOn(wood0,ground).
isOn(wood1,wood0).
isOn(wood1,wood4).
isOn(wood2,wood1).
isOn(wood3,wood2).
isOn(wood4,ground).
isOver(ice6,pig1).
isOver(ice7,pig1).
isOver(ice8,pig1).
isOver(wood1,pig0).
isOver(wood2,pig0).
isOver(wood3,pig0).
pig(pig0,270,389,9,9).
pig(pig1,397,269,9,9).
protects(struct1,pig0).
protects(struct3,pig1).
scene_scale(74,1.005).
shape(bluebird0,ball, 56,348.5,44,[3.75]).
shape(hill0,poly, 182,401,0,[2,[37,401],[37,402]]).
shape(hill1,poly, 486,402,614,[4,[332,401],[333,402],[639,402],[639,400]]).
shape(hill2,poly, 398,294,2666,[4,[355,280],[356,310],[441,309],[440,279]]).
shape(ice5,rect, 374.5,267,276,[23,24,1.57079633]).
shape(ice6,rect, 399,252,348,[6,58,0]).
shape(ice7,rect, 399,244.5,309,[6.43678999,48.13336869,3.11017673]).
shape(ice8,rect, 398.5,238,138,[6,23,0]).
shape(ice9,poly, 424,267,253,[5,[413,255],[414,277],[433,278],[435,257],[418,255]]).
shape(pig0,ball, 274.5,393,122,[6.25]).
shape(pig1,ball, 401.5,273,122,[6.25]).
shape(wood0,rect, 247.5,387,276,[23,24,1.57079633]).
shape(wood1,rect, 272,371,348,[6,58,0]).
shape(wood2,rect, 272,364,288,[6,48,0]).
shape(wood3,rect, 271.5,357.5,115,[5,23,0]).
shape(wood4,rect, 296.5,387,276,[23,24,1.57079633]).
situation_name('Level1-2').
slingshotPivot(56.08,349.72).
structure(struct0).
structure(struct1).
structure(struct2).
structure(struct3).

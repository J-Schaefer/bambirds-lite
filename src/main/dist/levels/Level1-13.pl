belongsTo(pig0,struct1).
belongsTo(pig1,struct2).
belongsTo(pig2,struct3).
belongsTo(wood0,struct0).
belongsTo(wood1,struct0).
bird(redbird1).
bird(yellowbird0).
birdOrder(redbird1,1).
birdOrder(yellowbird0,0).
canCollapse(struct0,struct1).
canCollapse(struct1,struct0).
canCollapse(struct1,struct2).
canCollapse(struct2,struct1).
canCollapse(struct2,struct3).
canCollapse(struct3,struct2).
collapsesInDirection(struct0,struct1,away).
collapsesInDirection(struct1,struct0,towards).
collapsesInDirection(struct1,struct2,away).
collapsesInDirection(struct2,struct1,towards).
collapsesInDirection(struct2,struct3,away).
collapsesInDirection(struct3,struct2,towards).
ground_plane(384).
hasColor(redbird1,red).
hasColor(yellowbird0,yellow).
hasForm(wood0,bar).
hasForm(wood1,bar).
hasMaterial(pig0,pork,396,337,9,9).
hasMaterial(pig1,pork,405,282,8,8).
hasMaterial(pig2,pork,414,337,8,8).
hasMaterial(wood0,wood,374,267,5,24).
hasMaterial(wood1,wood,380,267,6,24).
hasOrientation(wood0,vertical).
hasOrientation(wood1,vertical).
hasSize(hill0,small).
hasSize(hill1,medium).
hasSize(pig0,medium).
hasSize(pig1,medium).
hasSize(pig2,medium).
hasSize(redbird1,medium).
hasSize(wood0,big).
hasSize(wood1,big).
hasSize(yellowbird0,medium).
hill(hill0,37,401,0,1).
hill(hill1,332,236,307,166).
isAnchorPointFor(pig0,struct1).
isAnchorPointFor(pig1,struct2).
isAnchorPointFor(pig2,struct3).
isAnchorPointFor(wood0,struct0).
isCollapsable(struct0).
isCollapsable(struct1).
isCollapsable(struct2).
isCollapsable(struct3).
isLeft(wood0,wood1).
isOn(pig0,hill1).
isOn(pig1,hill1).
isOn(pig2,hill1).
isOn(wood0,hill1).
isOn(wood1,hill1).
isOver(hill1,pig0).
isOver(hill1,pig1).
isOver(hill1,pig2).
isOver(pig1,pig0).
isOver(pig1,pig2).
isRight(wood1,wood0).
isTower(struct0).
pig(pig0,396,337,9,9).
pig(pig1,405,282,8,8).
pig(pig2,414,337,8,8).
protects(struct2,pig0).
protects(struct2,pig2).
scene_scale(73,1.005).
shape(hill0,poly, 182,402,0,[2,[37,401],[37,402]]).
shape(hill1,poly, 486,320,50962,[17,[332,401],[333,402],[639,402],[639,400],[482,401],[460,398],[459,236],[374,237],[375,266],[429,267],[428,291],[374,292],[375,322],[429,323],[428,347],[374,348],[373,399]]).
shape(pig0,ball, 400.5,341,122,[6.25]).
shape(pig1,ball, 409,286,113,[6]).
shape(pig2,ball, 418.5,341.5,113,[6]).
shape(redbird1,ball, 83.5,392,86,[5.25]).
shape(wood0,rect, 376.5,279,120,[5,24,1.57079633]).
shape(wood1,rect, 383,279,144,[6,24,1.57079633]).
shape(yellowbird0,ball, 105,345,50,[4]).
situation_name('Level1-13').
slingshotPivot(105.52,349.18).
structure(struct0).
structure(struct1).
structure(struct2).
structure(struct3).

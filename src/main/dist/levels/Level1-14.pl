belongsTo(pig0,struct0).
belongsTo(stone0,struct0).
belongsTo(stone1,struct0).
belongsTo(stone10,struct0).
belongsTo(stone2,struct0).
belongsTo(stone3,struct0).
belongsTo(stone4,struct0).
belongsTo(stone5,struct0).
belongsTo(stone6,struct0).
belongsTo(stone8,struct0).
belongsTo(stone9,struct0).
belongsTo(wood11,struct0).
belongsTo(wood7,struct0).
bird(bluebird2).
bird(redbird1).
bird(yellowbird0).
birdOrder(bluebird2,2).
birdOrder(redbird1,1).
birdOrder(yellowbird0,0).
ground_plane(384).
hasColor(bluebird2,blue).
hasColor(redbird1,red).
hasColor(yellowbird0,yellow).
hasForm(stone0,bar).
hasForm(stone1,bar).
hasForm(stone10,block).
hasForm(stone2,bar).
hasForm(stone3,bar).
hasForm(stone4,block).
hasForm(stone5,block).
hasForm(stone6,block).
hasForm(stone8,block).
hasForm(stone9,block).
hasForm(wood11,bar).
hasForm(wood7,bar).
hasMaterial(pig0,pork,243,273,8,8).
hasMaterial(stone0,stone,206,288,10,21).
hasMaterial(stone1,stone,206,266,10,21).
hasMaterial(stone10,stone,228,244,11,21).
hasMaterial(stone2,stone,206,244,10,21).
hasMaterial(stone3,stone,208,310,51,5).
hasMaterial(stone4,stone,215,265,12,22).
hasMaterial(stone5,stone,215,244,13,21).
hasMaterial(stone6,stone,216,289,12,20).
hasMaterial(stone8,stone,228,288,11,21).
hasMaterial(stone9,stone,228,266,11,21).
hasMaterial(wood11,wood,256,316,5,43).
hasMaterial(wood7,wood,217,316,5,43).
hasOrientation(stone0,vertical).
hasOrientation(stone1,vertical).
hasOrientation(stone10,vertical).
hasOrientation(stone2,vertical).
hasOrientation(stone3,horizontal).
hasOrientation(stone4,vertical).
hasOrientation(stone5,vertical).
hasOrientation(stone6,vertical).
hasOrientation(stone8,vertical).
hasOrientation(stone9,vertical).
hasOrientation(wood11,vertical).
hasOrientation(wood7,vertical).
hasSize(bluebird2,medium).
hasSize(hill0,big).
hasSize(pig0,medium).
hasSize(redbird1,medium).
hasSize(stone0,small).
hasSize(stone1,small).
hasSize(stone10,medium).
hasSize(stone2,small).
hasSize(stone3,big).
hasSize(stone4,medium).
hasSize(stone5,medium).
hasSize(stone6,medium).
hasSize(stone8,medium).
hasSize(stone9,medium).
hasSize(wood11,big).
hasSize(wood7,big).
hasSize(yellowbird0,medium).
hill(hill0,37,242,602,141).
isAnchorPointFor(stone0,struct0).
isBelow(stone0,stone1).
isBelow(stone0,stone4).
isBelow(stone1,stone2).
isBelow(stone1,stone4).
isBelow(stone1,stone5).
isBelow(stone3,stone0).
isBelow(stone3,stone6).
isBelow(stone3,stone8).
isBelow(stone4,stone2).
isBelow(stone4,stone5).
isBelow(stone6,stone4).
isBelow(stone8,stone9).
isBelow(stone9,stone10).
isBelow(wood11,stone3).
isBelow(wood7,stone3).
isCollapsable(struct0).
isLeft(stone0,stone6).
isLeft(stone2,stone5).
isLeft(stone4,stone9).
isLeft(stone5,stone10).
isLeft(stone6,stone8).
isLeft(stone8,bluebird2).
isLeft(stone9,pig0).
isOn(pig0,hill0).
isOn(stone0,hill0).
isOn(stone0,stone3).
isOn(stone1,hill0).
isOn(stone1,stone0).
isOn(stone10,hill0).
isOn(stone10,stone9).
isOn(stone2,hill0).
isOn(stone2,stone1).
isOn(stone2,stone4).
isOn(stone3,hill0).
isOn(stone3,wood11).
isOn(stone3,wood7).
isOn(stone4,hill0).
isOn(stone4,stone0).
isOn(stone4,stone6).
isOn(stone5,hill0).
isOn(stone5,stone1).
isOn(stone5,stone4).
isOn(stone6,hill0).
isOn(stone6,stone3).
isOn(stone8,hill0).
isOn(stone8,stone3).
isOn(stone9,hill0).
isOn(stone9,stone8).
isOn(wood11,hill0).
isOn(wood7,hill0).
isOver(hill0,pig0).
isOver(stone10,pig0).
isOver(stone9,pig0).
isRight(pig0,stone9).
isRight(stone10,stone5).
isRight(stone4,stone1).
isRight(stone5,stone2).
isRight(stone6,stone0).
isRight(stone8,stone6).
isRight(stone9,stone4).
isTower(struct0).
pig(pig0,243,273,8,8).
protects(struct0,pig0).
scene_scale(64,1.005).
shape(bluebird2,ball, 239,298.5,9,[1.75]).
shape(hill0,poly, 338,313,84882,[18,[37,381],[38,382],[639,383],[639,381],[326,379],[325,242],[239,243],[240,269],[253,270],[252,282],[239,283],[240,307],[261,308],[262,326],[299,327],[298,359],[185,360],[184,381]]).
shape(pig0,ball, 246.5,276.5,95,[5.5]).
shape(redbird1,ball, 68,373.5,70,[4.75]).
shape(stone0,rect, 211,298.5,210,[10,21,1.57079633]).
shape(stone1,rect, 211,276.5,210,[10,21,1.57079633]).
shape(stone10,rect, 233.5,254.5,231,[11,21,1.57079633]).
shape(stone2,rect, 211,254.5,210,[10,21,1.57079633]).
shape(stone3,rect, 233.5,312.5,255,[5,51,0]).
shape(stone4,rect, 221,276,264,[12,22,1.57079633]).
shape(stone5,rect, 221.5,254.5,273,[13,21,1.57079633]).
shape(stone6,rect, 222,299,240,[12,20,1.57079633]).
shape(stone8,rect, 233.5,298.5,231,[11,21,1.57079633]).
shape(stone9,rect, 233.5,276.5,231,[11,21,1.57079633]).
shape(wood11,rect, 258.5,337.5,215,[5,43,1.57079633]).
shape(wood7,rect, 219.5,337.5,215,[5,43,1.57079633]).
shape(yellowbird0,ball, 87.5,333,44,[3.75]).
situation_name('Level1-14').
slingshotPivot(87.4,336.1).
structure(struct0).

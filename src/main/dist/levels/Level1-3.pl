belongsTo(ice3,struct0).
belongsTo(ice4,struct0).
belongsTo(ice8,struct1).
belongsTo(pig0,struct3).
belongsTo(stone0,struct2).
belongsTo(stone10,struct2).
belongsTo(stone2,struct2).
belongsTo(stone5,struct2).
belongsTo(stone7,struct2).
belongsTo(stone9,struct2).
belongsTo(wood1,struct2).
belongsTo(wood6,struct2).
bird(redbird0).
birdOrder(redbird0,0).
canCollapse(struct1,struct2).
canCollapse(struct2,struct1).
canCollapse(struct2,struct3).
collapsesInDirection(struct0,struct1,away).
collapsesInDirection(struct1,struct0,towards).
collapsesInDirection(struct1,struct2,away).
collapsesInDirection(struct2,struct1,towards).
collapsesInDirection(struct2,struct3,away).
collapsesInDirection(struct3,struct2,towards).
ground_plane(384).
hasColor(redbird0,red).
hasForm(ice3,block).
hasForm(ice4,bar).
hasForm(ice8,block).
hasForm(stone0,bar).
hasForm(stone10,bar).
hasForm(stone2,bar).
hasForm(stone5,bar).
hasForm(stone7,block).
hasForm(stone9,bar).
hasForm(wood1,bar).
hasForm(wood6,bar).
hasMaterial(ice3,ice,269,225,8,9).
hasMaterial(ice4,ice,271,208,4,17).
hasMaterial(ice8,ice,301,225,8,10).
hasMaterial(pig0,pork,351,349,6,6).
hasMaterial(stone0,stone,251,194,17,4).
hasMaterial(stone10,stone,316,199,4,35).
hasMaterial(stone2,stone,257,199,4,35).
hasMaterial(stone5,stone,280,194,17,4).
hasMaterial(stone7,stone,296,173,17,15).
hasMaterial(stone9,stone,310,194,16,4).
hasMaterial(wood1,wood,253,189,35,4).
hasMaterial(wood6,wood,289,189,35,4).
hasOrientation(ice3,vertical).
hasOrientation(ice4,vertical).
hasOrientation(ice8,vertical).
hasOrientation(stone0,horizontal).
hasOrientation(stone10,vertical).
hasOrientation(stone2,vertical).
hasOrientation(stone5,horizontal).
hasOrientation(stone7,horizontal).
hasOrientation(stone9,horizontal).
hasOrientation(wood1,horizontal).
hasOrientation(wood6,horizontal).
hasSize(hill0,small).
hasSize(hill1,big).
hasSize(hill2,big).
hasSize(hill3,big).
hasSize(ice3,medium).
hasSize(ice4,big).
hasSize(ice8,medium).
hasSize(pig0,medium).
hasSize(redbird0,medium).
hasSize(stone0,big).
hasSize(stone10,big).
hasSize(stone2,big).
hasSize(stone5,big).
hasSize(stone7,medium).
hasSize(stone9,big).
hasSize(wood1,big).
hasSize(wood6,big).
hill(hill0,0,357,0,3).
hill(hill1,257,234,63,22).
hill(hill2,283,356,356,3).
hill(hill3,298,298,22,58).
isAnchorPointFor(ice3,struct0).
isAnchorPointFor(ice8,struct1).
isAnchorPointFor(pig0,struct3).
isAnchorPointFor(stone0,struct2).
isBelow(ice3,ice4).
isBelow(stone0,wood1).
isBelow(stone10,stone9).
isBelow(stone2,stone0).
isBelow(stone5,wood1).
isBelow(stone5,wood6).
isBelow(stone9,wood6).
isBelow(wood6,stone7).
isCollapsable(struct0).
isCollapsable(struct1).
isCollapsable(struct2).
isCollapsable(struct3).
isLeft(wood1,wood6).
isOn(ice3,hill1).
isOn(ice4,ice3).
isOn(ice8,hill1).
isOn(pig0,hill2).
isOn(stone0,stone2).
isOn(stone10,hill1).
isOn(stone2,hill1).
isOn(stone5,ground).
isOn(stone7,wood6).
isOn(stone9,stone10).
isOn(wood1,stone0).
isOn(wood1,stone5).
isOn(wood6,stone5).
isOn(wood6,stone9).
isRight(wood6,wood1).
isTower(struct0).
isTower(struct1).
pig(pig0,351,349,6,6).
scene_scale(53,1.005).
shape(hill0,poly, 141,358,0,[2,[0,357],[0,360]]).
shape(hill1,poly, 289,246,1386,[4,[257,235],[257,255],[320,256],[319,234]]).
shape(hill2,poly, 461,358,1068,[6,[283,358],[284,359],[639,358],[639,356],[349,358],[324,356]]).
shape(hill3,poly, 309,328,1276,[4,[298,299],[298,355],[320,356],[319,298]]).
shape(ice3,rect, 273,229.5,72,[8,9,1.57079633]).
shape(ice4,rect, 273,216.5,68,[4,17,1.57079633]).
shape(ice8,rect, 305,230,80,[8,10,1.57079633]).
shape(pig0,ball, 354.5,352.5,63,[4.5]).
shape(redbird0,ball, 126.5,316.5,38,[3.5]).
shape(stone0,rect, 259.5,196,68,[4,17,0]).
shape(stone10,rect, 318,216.5,140,[4,35,1.57079633]).
shape(stone2,rect, 259,216.5,140,[4,35,1.57079633]).
shape(stone5,rect, 288.5,196,68,[4,17,0]).
shape(stone7,poly, 305,180,127.5,[4,[296,173],[298,188],[311,188],[313,173]]).
shape(stone9,rect, 318,196,64,[4,16,0]).
shape(wood1,rect, 270.5,191,140,[4,35,0]).
shape(wood6,rect, 306.5,191,140,[4,35,0]).
situation_name('Level1-3').
slingshotPivot(126.28,320.02).
structure(struct0).
structure(struct1).
structure(struct2).
structure(struct3).

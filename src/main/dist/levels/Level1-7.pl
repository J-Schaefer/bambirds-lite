belongsTo(pig0,struct0).
belongsTo(pig1,struct2).
belongsTo(pig2,struct3).
belongsTo(pig3,struct4).
belongsTo(stone0,struct1).
belongsTo(stone3,struct5).
belongsTo(wood1,struct2).
belongsTo(wood2,struct3).
bird(redbird0).
bird(redbird1).
bird(redbird2).
bird(redbird3).
birdOrder(redbird0,0).
birdOrder(redbird1,1).
birdOrder(redbird2,2).
birdOrder(redbird3,3).
canCollapse(struct0,struct1).
canCollapse(struct1,struct0).
canCollapse(struct2,struct1).
canCollapse(struct4,struct3).
canCollapse(struct4,struct5).
canCollapse(struct5,struct4).
collapsesInDirection(struct0,struct1,away).
collapsesInDirection(struct1,struct0,towards).
collapsesInDirection(struct1,struct2,away).
collapsesInDirection(struct2,struct1,towards).
collapsesInDirection(struct2,struct3,away).
collapsesInDirection(struct3,struct2,towards).
collapsesInDirection(struct3,struct4,away).
collapsesInDirection(struct4,struct3,towards).
collapsesInDirection(struct4,struct5,away).
collapsesInDirection(struct5,struct4,towards).
ground_plane(384).
hasColor(redbird0,red).
hasColor(redbird1,red).
hasColor(redbird2,red).
hasColor(redbird3,red).
hasForm(stone0,bar).
hasForm(stone3,bar).
hasForm(wood1,bar).
hasForm(wood2,bar).
hasMaterial(pig0,pork,397,297,6,6).
hasMaterial(pig1,pork,426,297,6,6).
hasMaterial(pig2,pork,455,297,6,6).
hasMaterial(pig3,pork,501,280,6,6).
hasMaterial(stone0,stone,399,309,4,16).
hasMaterial(stone3,stone,503,293,4,33).
hasMaterial(wood1,wood,422,305,16,4).
hasMaterial(wood2,wood,451,305,16,4).
hasOrientation(stone0,vertical).
hasOrientation(stone3,vertical).
hasOrientation(wood1,horizontal).
hasOrientation(wood2,horizontal).
hasSize(hill0,big).
hasSize(hill1,big).
hasSize(pig0,medium).
hasSize(pig1,medium).
hasSize(pig2,medium).
hasSize(pig3,medium).
hasSize(redbird0,medium).
hasSize(redbird1,medium).
hasSize(redbird2,medium).
hasSize(redbird3,medium).
hasSize(stone0,small).
hasSize(stone3,big).
hasSize(wood1,small).
hasSize(wood2,small).
hill(hill0,143,335,63,15).
hill(hill1,368,243,271,108).
isAnchorPointFor(pig0,struct0).
isAnchorPointFor(pig3,struct4).
isAnchorPointFor(stone0,struct1).
isAnchorPointFor(stone3,struct5).
isAnchorPointFor(wood1,struct2).
isAnchorPointFor(wood2,struct3).
isBelow(wood1,pig1).
isBelow(wood2,pig2).
isCollapsable(struct0).
isCollapsable(struct1).
isCollapsable(struct2).
isCollapsable(struct3).
isCollapsable(struct4).
isCollapsable(struct5).
isOn(pig0,hill1).
isOn(pig1,hill1).
isOn(pig1,wood1).
isOn(pig2,hill1).
isOn(pig2,wood2).
isOn(pig3,hill1).
isOn(stone0,hill1).
isOn(stone3,hill1).
isOn(wood1,hill1).
isOn(wood2,hill1).
isOver(hill1,pig0).
isOver(hill1,pig1).
isOver(hill1,pig2).
isOver(hill1,pig3).
isTower(struct1).
isTower(struct5).
pig(pig0,397,297,6,6).
pig(pig1,426,297,6,6).
pig(pig2,455,297,6,6).
pig(pig3,501,280,6,6).
scene_scale(50,1.005).
shape(hill0,poly, 256,344,945,[5,[143,348],[143,349],[206,350],[191,335],[149,336]]).
shape(hill1,poly, 504,298,29268,[14,[368,350],[369,351],[639,351],[639,349],[622,350],[600,327],[597,328],[593,321],[639,275],[639,269],[613,243],[529,327],[394,328],[373,349]]).
shape(pig0,ball, 400.5,300.5,63,[4.5]).
shape(pig1,ball, 429,300,63,[4.5]).
shape(pig2,ball, 458.5,300.5,63,[4.5]).
shape(pig3,ball, 504.5,283.5,63,[4.5]).
shape(redbird0,ball, 180,297.5,33,[3.25]).
shape(redbird1,ball, 164,330,38,[3.5]).
shape(redbird2,ball, 132.5,344.5,50,[4]).
shape(redbird3,ball, 117,344,38,[3.5]).
shape(stone0,rect, 401,317,64,[4,16,1.57079633]).
shape(stone3,rect, 505,309.5,132,[4,33,1.57079633]).
shape(wood1,rect, 430,307,64,[4,16,0]).
shape(wood2,rect, 459,307,64,[4,16,0]).
situation_name('Level1-7').
slingshotPivot(179.72,300.48).
structure(struct0).
structure(struct1).
structure(struct2).
structure(struct3).
structure(struct4).
structure(struct5).

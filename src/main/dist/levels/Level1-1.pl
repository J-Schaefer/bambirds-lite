belongsTo(pig0,struct3).
belongsTo(stone0,struct2).
belongsTo(tnt0,struct0).
belongsTo(tnt1,struct1).
bird(redbird0).
birdOrder(redbird0,0).
canCollapse(struct2,struct1).
canCollapse(struct2,struct3).
canCollapse(struct3,struct2).
canExplode(tnt0,tnt0).
canExplode(tnt0,tnt1).
canExplode(tnt1,stone0).
canExplode(tnt1,tnt0).
canExplode(tnt1,tnt1).
collapsesInDirection(struct0,struct1,away).
collapsesInDirection(struct1,struct0,towards).
collapsesInDirection(struct1,struct2,away).
collapsesInDirection(struct2,struct1,towards).
collapsesInDirection(struct2,struct3,away).
collapsesInDirection(struct3,struct2,towards).
ground_plane(384).
hasColor(redbird0,red).
hasForm(stone0,ball).
hasMaterial(pig0,pork,320,388,15,15).
hasMaterial(stone0,stone,283,219,15,15).
hasMaterial(tnt0,tnt,217,220,20,20).
hasMaterial(tnt1,tnt,248,220,21,20).
hasOrientation(stone0,horizontal).
hasSize(hill0,small).
hasSize(pig0,medium).
hasSize(redbird0,medium).
hasSize(stone0,medium).
hasSize(tnt0,small).
hasSize(tnt1,medium).
hill(hill0,37,161,602,250).
isAnchorPointFor(pig0,struct3).
isAnchorPointFor(stone0,struct2).
isAnchorPointFor(tnt0,struct0).
isAnchorPointFor(tnt1,struct1).
isCollapsable(struct0).
isCollapsable(struct1).
isCollapsable(struct2).
isCollapsable(struct3).
isOn(pig0,hill0).
isOn(stone0,hill0).
isOn(tnt0,hill0).
isOn(tnt1,hill0).
isOver(hill0,pig0).
isOver(hill0,stone0).
isOver(hill0,tnt0).
isOver(hill0,tnt1).
pig(pig0,320,388,15,15).
scene_scale(77,1.005).
shape(hill0,poly, 338,287,150500,[21,[37,409],[38,410],[639,411],[639,407],[613,409],[577,407],[545,408],[526,407],[494,409],[376,406],[375,161],[208,162],[209,193],[344,194],[343,407],[308,406],[307,239],[217,240],[218,271],[276,272],[275,408]]).
shape(pig0,ball, 327.5,396,363,[10.75]).
shape(redbird0,ball, 129.5,348.5,78,[5]).
shape(stone0,ball, 291.5,227.5,380,[11]).
shape(tnt0,rect, 227,230,400,[20,20,0]).
shape(tnt1,rect, 258.5,230,420,[20,21,0]).
situation_name('Level1-1').
slingshotPivot(129.08,353.72).
structure(struct0).
structure(struct1).
structure(struct2).
structure(struct3).

belongsTo(pig0,struct1).
belongsTo(pig1,struct2).
belongsTo(pig2,struct3).
belongsTo(pig3,struct4).
belongsTo(pig4,struct5).
belongsTo(pig5,struct6).
belongsTo(pig6,struct6).
belongsTo(stone0,struct0).
belongsTo(stone1,struct0).
belongsTo(stone10,struct0).
belongsTo(stone11,struct0).
belongsTo(stone17,struct0).
belongsTo(stone19,struct0).
belongsTo(stone2,struct0).
belongsTo(stone20,struct0).
belongsTo(stone21,struct0).
belongsTo(stone22,struct0).
belongsTo(stone23,struct0).
belongsTo(stone24,struct0).
belongsTo(stone25,struct0).
belongsTo(stone3,struct0).
belongsTo(stone4,struct0).
belongsTo(stone5,struct0).
belongsTo(stone6,struct0).
belongsTo(stone7,struct0).
belongsTo(stone8,struct0).
belongsTo(stone9,struct0).
belongsTo(wood12,struct0).
belongsTo(wood13,struct0).
belongsTo(wood14,struct0).
belongsTo(wood15,struct0).
belongsTo(wood16,struct0).
belongsTo(wood18,struct0).
belongsTo(wood26,struct6).
belongsTo(wood27,struct6).
bird(yellowbird0).
bird(yellowbird1).
bird(yellowbird2).
bird(yellowbird3).
birdOrder(yellowbird0,0).
birdOrder(yellowbird1,1).
birdOrder(yellowbird2,2).
birdOrder(yellowbird3,3).
canCollapse(struct0,struct1).
canCollapse(struct1,struct0).
canCollapse(struct1,struct2).
canCollapse(struct2,struct1).
canCollapse(struct3,struct4).
canCollapse(struct4,struct5).
canCollapse(struct6,struct5).
collapsesInDirection(struct0,struct1,away).
collapsesInDirection(struct1,struct0,towards).
collapsesInDirection(struct1,struct2,away).
collapsesInDirection(struct2,struct1,towards).
collapsesInDirection(struct2,struct3,away).
collapsesInDirection(struct3,struct2,towards).
collapsesInDirection(struct3,struct4,away).
collapsesInDirection(struct4,struct3,towards).
collapsesInDirection(struct4,struct5,away).
collapsesInDirection(struct5,struct4,towards).
collapsesInDirection(struct5,struct6,away).
collapsesInDirection(struct6,struct5,towards).
ground_plane(384).
hasColor(yellowbird0,yellow).
hasColor(yellowbird1,yellow).
hasColor(yellowbird2,yellow).
hasColor(yellowbird3,yellow).
hasForm(stone0,block).
hasForm(stone1,cube).
hasForm(stone10,block).
hasForm(stone11,bar).
hasForm(stone17,block).
hasForm(stone19,block).
hasForm(stone2,block).
hasForm(stone20,bar).
hasForm(stone21,cube).
hasForm(stone22,block).
hasForm(stone23,block).
hasForm(stone24,bar).
hasForm(stone25,block).
hasForm(stone3,block).
hasForm(stone4,block).
hasForm(stone5,block).
hasForm(stone6,bar).
hasForm(stone7,block).
hasForm(stone8,bar).
hasForm(stone9,bar).
hasForm(wood12,block).
hasForm(wood13,cube).
hasForm(wood14,block).
hasForm(wood15,block).
hasForm(wood16,block).
hasForm(wood18,block).
hasForm(wood26,bar).
hasForm(wood27,bar).
hasMaterial(pig0,pork,421,292,12,12).
hasMaterial(pig1,pork,444,293,12,12).
hasMaterial(pig2,pork,479,293,12,12).
hasMaterial(pig3,pork,510,306,12,12).
hasMaterial(pig4,pork,564,334,5,5).
hasMaterial(pig5,pork,583,328,10,10).
hasMaterial(pig6,pork,604,326,12,12).
hasMaterial(stone0,stone,346,292,11,14).
hasMaterial(stone1,stone,349,193,27,27).
hasMaterial(stone10,stone,368,197,19,10).
hasMaterial(stone11,stone,369,256,23,4).
hasMaterial(stone17,stone,374,291,8,15).
hasMaterial(stone19,stone,380,196,23,24).
hasMaterial(stone2,stone,357,196,18,10).
hasMaterial(stone20,stone,381,198,17,8).
hasMaterial(stone21,stone,382,291,15,15).
hasMaterial(stone22,stone,384,220,8,4).
hasMaterial(stone23,stone,384,206,12,14).
hasMaterial(stone24,stone,387,260,4,31).
hasMaterial(stone25,stone,398,295,13,11).
hasMaterial(stone3,stone,358,301,9,5).
hasMaterial(stone4,stone,358,294,4,6).
hasMaterial(stone5,stone,358,208,13,11).
hasMaterial(stone6,stone,364,260,4,31).
hasMaterial(stone7,stone,364,220,7,4).
hasMaterial(stone8,stone,368,291,6,15).
hasMaterial(stone9,stone,368,208,19,9).
hasMaterial(wood12,wood,370,241,14,15).
hasMaterial(wood13,wood,370,226,14,14).
hasMaterial(wood14,wood,370,188,16,8).
hasMaterial(wood15,wood,371,278,15,12).
hasMaterial(wood16,wood,371,260,14,15).
hasMaterial(wood18,wood,375,180,6,8).
hasMaterial(wood26,wood,576,310,4,30).
hasMaterial(wood27,wood,596,303,4,37).
hasOrientation(stone0,vertical).
hasOrientation(stone1,horizontal).
hasOrientation(stone10,horizontal).
hasOrientation(stone11,horizontal).
hasOrientation(stone17,vertical).
hasOrientation(stone19,vertical).
hasOrientation(stone2,horizontal).
hasOrientation(stone20,horizontal).
hasOrientation(stone21,horizontal).
hasOrientation(stone22,horizontal).
hasOrientation(stone23,vertical).
hasOrientation(stone24,vertical).
hasOrientation(stone25,horizontal).
hasOrientation(stone3,horizontal).
hasOrientation(stone4,vertical).
hasOrientation(stone5,horizontal).
hasOrientation(stone6,vertical).
hasOrientation(stone7,horizontal).
hasOrientation(stone8,vertical).
hasOrientation(stone9,horizontal).
hasOrientation(wood12,vertical).
hasOrientation(wood13,horizontal).
hasOrientation(wood14,horizontal).
hasOrientation(wood15,horizontal).
hasOrientation(wood16,vertical).
hasOrientation(wood18,vertical).
hasOrientation(wood26,vertical).
hasOrientation(wood27,vertical).
hasSize(hill0,big).
hasSize(pig0,medium).
hasSize(pig1,medium).
hasSize(pig2,medium).
hasSize(pig3,medium).
hasSize(pig4,medium).
hasSize(pig5,medium).
hasSize(pig6,medium).
hasSize(stone0,medium).
hasSize(stone1,small).
hasSize(stone10,medium).
hasSize(stone11,medium).
hasSize(stone17,medium).
hasSize(stone19,medium).
hasSize(stone2,medium).
hasSize(stone20,medium).
hasSize(stone21,small).
hasSize(stone22,medium).
hasSize(stone23,medium).
hasSize(stone24,big).
hasSize(stone25,medium).
hasSize(stone3,medium).
hasSize(stone4,medium).
hasSize(stone5,medium).
hasSize(stone6,big).
hasSize(stone7,medium).
hasSize(stone8,small).
hasSize(stone9,medium).
hasSize(wood12,medium).
hasSize(wood13,small).
hasSize(wood14,medium).
hasSize(wood15,medium).
hasSize(wood16,medium).
hasSize(wood18,medium).
hasSize(wood26,medium).
hasSize(wood27,big).
hasSize(yellowbird0,medium).
hasSize(yellowbird1,medium).
hasSize(yellowbird2,medium).
hasSize(yellowbird3,medium).
hill(hill0,25,271,614,72).
isAnchorPointFor(pig0,struct1).
isAnchorPointFor(pig1,struct2).
isAnchorPointFor(pig2,struct3).
isAnchorPointFor(pig3,struct4).
isAnchorPointFor(pig4,struct5).
isAnchorPointFor(stone0,struct0).
isAnchorPointFor(wood26,struct6).
isBelow(stone1,wood14).
isBelow(stone10,stone2).
isBelow(stone10,wood14).
isBelow(stone11,wood12).
isBelow(stone17,wood15).
isBelow(stone19,wood14).
isBelow(stone2,wood14).
isBelow(stone20,wood14).
isBelow(stone21,stone24).
isBelow(stone21,wood15).
isBelow(stone22,stone19).
isBelow(stone22,stone23).
isBelow(stone22,stone9).
isBelow(stone23,stone10).
isBelow(stone23,stone19).
isBelow(stone23,stone20).
isBelow(stone24,stone11).
isBelow(stone3,stone4).
isBelow(stone5,stone10).
isBelow(stone5,stone2).
isBelow(stone7,stone1).
isBelow(stone7,stone5).
isBelow(stone7,stone9).
isBelow(stone8,wood15).
isBelow(stone9,stone10).
isBelow(stone9,stone2).
isBelow(stone9,stone20).
isBelow(wood12,wood13).
isBelow(wood13,stone7).
isBelow(wood14,wood18).
isBelow(wood15,wood16).
isBelow(wood16,stone11).
isBelow(wood16,wood12).
isCollapsable(struct0).
isCollapsable(struct1).
isCollapsable(struct2).
isCollapsable(struct3).
isCollapsable(struct4).
isCollapsable(struct5).
isCollapsable(struct6).
isLeft(pig5,wood27).
isLeft(stone0,stone3).
isLeft(stone0,stone4).
isLeft(stone1,stone10).
isLeft(stone1,stone19).
isLeft(stone1,stone9).
isLeft(stone10,stone20).
isLeft(stone17,stone21).
isLeft(stone21,stone25).
isLeft(stone3,stone8).
isLeft(stone5,stone9).
isLeft(stone6,wood15).
isLeft(stone6,wood16).
isLeft(stone8,stone17).
isLeft(wood15,stone24).
isLeft(wood16,stone24).
isLeft(wood26,pig5).
isLeft(wood27,pig6).
isOn(pig0,hill0).
isOn(pig1,hill0).
isOn(pig2,hill0).
isOn(pig3,hill0).
isOn(pig4,hill0).
isOn(pig5,hill0).
isOn(pig6,hill0).
isOn(stone0,hill0).
isOn(stone1,stone7).
isOn(stone10,stone1).
isOn(stone10,stone19).
isOn(stone10,stone23).
isOn(stone10,stone5).
isOn(stone10,stone9).
isOn(stone11,stone24).
isOn(stone11,wood16).
isOn(stone17,hill0).
isOn(stone19,stone22).
isOn(stone2,stone1).
isOn(stone2,stone10).
isOn(stone2,stone5).
isOn(stone2,stone9).
isOn(stone20,stone10).
isOn(stone20,stone19).
isOn(stone20,stone23).
isOn(stone20,stone9).
isOn(stone21,hill0).
isOn(stone22,ground).
isOn(stone23,stone22).
isOn(stone24,hill0).
isOn(stone24,stone21).
isOn(stone25,hill0).
isOn(stone3,hill0).
isOn(stone4,hill0).
isOn(stone4,stone3).
isOn(stone5,stone1).
isOn(stone5,stone7).
isOn(stone6,hill0).
isOn(stone7,wood13).
isOn(stone8,hill0).
isOn(stone9,stone1).
isOn(stone9,stone19).
isOn(stone9,stone22).
isOn(stone9,stone23).
isOn(stone9,stone5).
isOn(stone9,stone7).
isOn(wood12,stone11).
isOn(wood12,wood16).
isOn(wood13,wood12).
isOn(wood14,stone1).
isOn(wood14,stone10).
isOn(wood14,stone19).
isOn(wood14,stone2).
isOn(wood14,stone20).
isOn(wood15,hill0).
isOn(wood15,stone17).
isOn(wood15,stone21).
isOn(wood15,stone8).
isOn(wood16,hill0).
isOn(wood16,wood15).
isOn(wood18,wood14).
isOn(wood26,hill0).
isOn(wood27,hill0).
isOver(hill0,pig0).
isOver(hill0,pig1).
isOver(hill0,pig2).
isOver(hill0,pig3).
isOver(hill0,pig4).
isOver(hill0,pig5).
isOver(hill0,pig6).
isOver(wood26,pig5).
isOver(wood27,pig5).
isOver(wood27,pig6).
isRight(pig5,wood26).
isRight(pig6,wood27).
isRight(stone1,stone2).
isRight(stone1,stone5).
isRight(stone17,stone8).
isRight(stone19,stone1).
isRight(stone19,stone10).
isRight(stone19,stone20).
isRight(stone19,stone23).
isRight(stone19,stone9).
isRight(stone21,stone17).
isRight(stone23,stone9).
isRight(stone24,wood15).
isRight(stone24,wood16).
isRight(stone25,stone21).
isRight(stone3,stone0).
isRight(stone4,stone0).
isRight(stone8,stone3).
isRight(wood15,stone6).
isRight(wood16,stone6).
isRight(wood27,pig5).
isTower(struct0).
pig(pig0,421,292,12,12).
pig(pig1,444,293,12,12).
pig(pig2,479,293,12,12).
pig(pig3,510,306,12,12).
pig(pig4,564,334,5,5).
pig(pig5,583,328,10,10).
pig(pig6,604,326,12,12).
protects(struct6,pig5).
protects(struct6,pig6).
scene_scale(44,1.005).
shape(hill0,poly, 332,307,44208,[27,[25,341],[26,342],[59,343],[639,342],[636,340],[540,339],[539,333],[525,332],[524,321],[507,320],[506,307],[342,308],[341,322],[326,323],[325,333],[310,334],[309,340],[210,339],[209,323],[191,322],[190,308],[174,307],[173,290],[155,289],[154,271],[81,273],[80,340]]).
shape(pig0,ball, 427.5,298,240,[8.75]).
shape(pig1,ball, 450,298.5,213,[8.25]).
shape(pig2,ball, 484.5,299,213,[8.25]).
shape(pig3,ball, 516,312.5,240,[8.75]).
shape(pig4,ball, 567,336.5,44,[3.75]).
shape(pig5,ball, 587.5,333,143,[6.75]).
shape(pig6,ball, 609.5,332,213,[8.25]).
shape(stone0,poly, 352,299,154,[4,[346,304],[346,305],[357,306],[357,292]]).
shape(stone1,rect, 362.5,206.5,121,[3.51157857,34.74218927,0.75398224]).
shape(stone10,poly, 378,202,190,[4,[368,206],[369,207],[387,205],[378,197]]).
shape(stone11,rect, 380.5,258,92,[4,23,0]).
shape(stone17,rect, 378,298.5,120,[8,15,1.57079633]).
shape(stone19,rect, 391.5,208,105,[3.53553391,29.69848481,2.35619449]).
shape(stone2,poly, 367,201,180,[5,[357,198],[361,202],[367,206],[375,198],[371,196]]).
shape(stone20,poly, 390,201,136,[4,[381,198],[386,203],[390,206],[398,198]]).
shape(stone21,poly, 390,299,112.5,[9,[382,293],[383,305],[397,306],[393,304],[387,303],[386,295],[390,294],[390,291],[387,292]]).
shape(stone22,rect, 388,222,32,[4,8,0]).
shape(stone23,poly, 391,213,168,[4,[384,219],[385,220],[396,219],[396,206]]).
shape(stone24,rect, 389,275.5,124,[4,31,1.57079633]).
shape(stone25,poly, 405,300,143,[4,[398,295],[399,306],[409,306],[411,305]]).
shape(stone3,rect, 362.5,303.5,45,[5,9,0]).
shape(stone4,poly, 364,296,24,[4,[358,294],[358,300],[362,299],[362,295]]).
shape(stone5,poly, 365,214,143,[3,[358,208],[359,219],[371,218]]).
shape(stone6,rect, 366,275.5,124,[4,31,1.57079633]).
shape(stone7,rect, 367.5,222,28,[4,7,0]).
shape(stone8,poly, 371,299,45,[5,[368,305],[369,306],[373,306],[374,292],[371,291]]).
shape(stone9,poly, 378,212,171,[4,[368,208],[374,214],[379,217],[387,209]]).
shape(wood12,poly, 378,248,105,[4,[370,243],[372,256],[384,254],[383,241]]).
shape(wood13,poly, 377,233,98,[4,[370,226],[370,238],[382,240],[384,227]]).
shape(wood14,rect, 378,192,128,[8,16,0]).
shape(wood15,poly, 379,284,90,[4,[371,278],[371,289],[384,290],[386,278]]).
shape(wood16,poly, 378,268,105,[8,[371,261],[372,275],[384,275],[385,272],[375,272],[373,265],[384,264],[384,260]]).
shape(wood18,rect, 378,184,48,[6,8,1.57079633]).
shape(wood26,rect, 578,325,120,[4,30,1.57079633]).
shape(wood27,rect, 598,321.5,148,[4,37,1.57079633]).
shape(yellowbird0,ball, 114,267,63,[4.5]).
shape(yellowbird1,ball, 95.5,267,56,[4.25]).
shape(yellowbird2,ball, 59.5,335,56,[4.25]).
shape(yellowbird3,ball, 51,333.5,56,[4.25]).
situation_name('Level1-15').
slingshotPivot(131.48,238.32).
structure(struct0).
structure(struct1).
structure(struct2).
structure(struct3).
structure(struct4).
structure(struct5).
structure(struct6).

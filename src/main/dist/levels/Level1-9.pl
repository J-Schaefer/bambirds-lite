belongsTo(ice0,struct2).
belongsTo(ice1,struct2).
belongsTo(ice2,struct2).
belongsTo(ice3,struct2).
belongsTo(ice4,struct2).
belongsTo(ice5,struct2).
belongsTo(pig0,struct0).
belongsTo(pig1,struct2).
belongsTo(stone6,struct1).
belongsTo(stone7,struct3).
belongsTo(stone9,struct5).
belongsTo(tnt0,struct4).
belongsTo(tnt1,struct4).
belongsTo(wood8,struct5).
bird(bluebird0).
bird(redbird2).
bird(yellowbird1).
birdOrder(bluebird0,0).
birdOrder(redbird2,2).
birdOrder(yellowbird1,1).
canCollapse(struct1,struct0).
canCollapse(struct1,struct2).
canCollapse(struct2,struct1).
canCollapse(struct2,struct3).
canCollapse(struct3,struct2).
canCollapse(struct3,struct4).
canCollapse(struct5,struct4).
canExplode(tnt0,hill1).
canExplode(tnt0,stone7).
canExplode(tnt0,tnt0).
canExplode(tnt0,tnt1).
canExplode(tnt1,hill1).
canExplode(tnt1,tnt0).
canExplode(tnt1,tnt1).
collapsesInDirection(struct0,struct1,away).
collapsesInDirection(struct1,struct0,towards).
collapsesInDirection(struct1,struct2,away).
collapsesInDirection(struct2,struct1,towards).
collapsesInDirection(struct2,struct3,away).
collapsesInDirection(struct3,struct2,towards).
collapsesInDirection(struct3,struct4,away).
collapsesInDirection(struct4,struct3,towards).
collapsesInDirection(struct4,struct5,away).
collapsesInDirection(struct5,struct4,towards).
ground_plane(384).
hasColor(bluebird0,blue).
hasColor(redbird2,red).
hasColor(yellowbird1,yellow).
hasForm(ice0,cube).
hasForm(ice1,block).
hasForm(ice2,cube).
hasForm(ice3,block).
hasForm(ice4,block).
hasForm(ice5,cube).
hasForm(stone6,bar).
hasForm(stone7,bar).
hasForm(stone9,ball).
hasForm(wood8,cube).
hasMaterial(ice0,ice,360,290,8,8).
hasMaterial(ice1,ice,360,281,9,8).
hasMaterial(ice2,ice,360,272,9,9).
hasMaterial(ice3,ice,368,290,8,9).
hasMaterial(ice4,ice,368,273,9,8).
hasMaterial(ice5,ice,369,281,8,8).
hasMaterial(pig0,pork,337,349,7,7).
hasMaterial(pig1,pork,378,283,13,13).
hasMaterial(stone6,stone,370,321,4,36).
hasMaterial(stone7,stone,400,321,4,36).
hasMaterial(stone9,stone,517,290,9,9).
hasMaterial(tnt0,tnt,422,343,15,15).
hasMaterial(tnt1,tnt,437,343,14,15).
hasMaterial(wood8,wood,511,303,8,8).
hasOrientation(ice0,horizontal).
hasOrientation(ice1,horizontal).
hasOrientation(ice2,horizontal).
hasOrientation(ice3,vertical).
hasOrientation(ice4,horizontal).
hasOrientation(ice5,horizontal).
hasOrientation(stone6,vertical).
hasOrientation(stone7,vertical).
hasOrientation(stone9,horizontal).
hasOrientation(wood8,horizontal).
hasSize(bluebird0,medium).
hasSize(hill0,small).
hasSize(hill1,big).
hasSize(hill2,medium).
hasSize(ice0,big).
hasSize(ice1,medium).
hasSize(ice2,big).
hasSize(ice3,medium).
hasSize(ice4,medium).
hasSize(ice5,big).
hasSize(pig0,medium).
hasSize(pig1,medium).
hasSize(redbird2,medium).
hasSize(stone6,big).
hasSize(stone7,big).
hasSize(stone9,medium).
hasSize(tnt0,big).
hasSize(tnt1,medium).
hasSize(wood8,big).
hasSize(yellowbird1,medium).
hill(hill0,0,357,0,3).
hill(hill1,283,294,356,65).
hill(hill2,306,257,109,99).
isAnchorPointFor(ice2,struct2).
isAnchorPointFor(pig0,struct0).
isAnchorPointFor(stone6,struct1).
isAnchorPointFor(stone7,struct3).
isAnchorPointFor(tnt0,struct4).
isAnchorPointFor(wood8,struct5).
isBelow(ice0,ice1).
isBelow(ice1,ice2).
isBelow(ice1,ice4).
isBelow(ice3,ice1).
isBelow(ice3,ice5).
isBelow(ice4,ice2).
isBelow(ice5,ice4).
isBelow(pig0,hill2).
isBelow(stone6,hill2).
isBelow(stone7,hill2).
isBelow(wood8,stone9).
isCollapsable(struct0).
isCollapsable(struct1).
isCollapsable(struct2).
isCollapsable(struct3).
isCollapsable(struct4).
isCollapsable(struct5).
isLeft(ice0,ice3).
isLeft(ice1,ice5).
isLeft(ice2,ice4).
isLeft(ice3,pig1).
isLeft(ice5,pig1).
isLeft(tnt0,tnt1).
isOn(ice0,hill1).
isOn(ice0,hill2).
isOn(ice1,hill2).
isOn(ice1,ice0).
isOn(ice1,ice3).
isOn(ice2,hill2).
isOn(ice2,ice1).
isOn(ice3,hill1).
isOn(ice3,hill2).
isOn(ice4,hill2).
isOn(ice4,ice1).
isOn(ice4,ice5).
isOn(ice5,hill2).
isOn(ice5,ice3).
isOn(pig0,hill1).
isOn(pig1,hill1).
isOn(pig1,hill2).
isOn(stone6,hill1).
isOn(stone7,hill1).
isOn(stone9,hill1).
isOn(stone9,wood8).
isOn(tnt0,hill1).
isOn(tnt1,hill1).
isOn(wood8,hill1).
isOver(hill1,pig0).
isOver(hill1,tnt0).
isOver(hill1,tnt1).
isOver(hill2,pig0).
isOver(hill2,pig1).
isOver(ice4,pig1).
isOver(ice5,pig1).
isRight(ice3,ice0).
isRight(ice5,ice1).
isRight(pig1,ice3).
isRight(pig1,ice5).
isRight(tnt1,tnt0).
isTower(struct1).
isTower(struct3).
isTower(struct5).
pig(pig0,337,349,7,7).
pig(pig1,378,283,13,13).
protects(struct2,pig1).
scene_scale(54,1.005).
shape(bluebird0,ball, 125,321.5,23,[2.75]).
shape(hill0,poly, 141,358,0,[2,[0,357],[0,360]]).
shape(hill1,poly, 461,327,23140,[12,[283,358],[284,359],[639,358],[639,356],[543,356],[542,296],[535,294],[520,311],[514,311],[468,357],[349,358],[324,356]]).
shape(hill2,poly, 361,307,10791,[14,[306,315],[306,355],[316,356],[316,318],[327,307],[415,306],[414,298],[404,297],[403,257],[375,258],[376,266],[395,267],[394,298],[322,298]]).
shape(ice0,rect, 364,294,64,[8,8,0]).
shape(ice1,rect, 364.5,285,72,[8,9,0]).
shape(ice2,rect, 364.5,276.5,81,[9,9,0]).
shape(ice3,rect, 372,294.5,72,[8,9,1.57079633]).
shape(ice4,rect, 372.5,277,72,[8,9,0]).
shape(ice5,rect, 373,285,64,[8,8,0]).
shape(pig0,ball, 340.5,352,70,[4.75]).
shape(pig1,ball, 384.5,289.5,254,[9]).
shape(redbird2,ball, 92,351,50,[4]).
shape(stone6,rect, 372,339,144,[4,36,1.57079633]).
shape(stone7,rect, 402,339,144,[4,36,1.57079633]).
shape(stone9,ball, 522.5,295,153,[7]).
shape(tnt0,rect, 429.5,350.5,225,[15,15,0]).
shape(tnt1,rect, 444,350.5,210,[15,14,0]).
shape(wood8,rect, 515,307,64,[8,8,0]).
shape(yellowbird1,ball, 104,350.5,70,[4.75]).
situation_name('Level1-10').
slingshotPivot(125.72,320.48).
structure(struct0).
structure(struct1).
structure(struct2).
structure(struct3).
structure(struct4).
structure(struct5).

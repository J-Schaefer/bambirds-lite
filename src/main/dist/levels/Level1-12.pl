belongsTo(ice10,struct0).
belongsTo(ice18,struct0).
belongsTo(ice19,struct0).
belongsTo(ice6,struct0).
belongsTo(ice7,struct0).
belongsTo(ice9,struct0).
belongsTo(pig0,struct0).
belongsTo(pig1,struct0).
belongsTo(pig2,struct0).
belongsTo(pig3,struct0).
belongsTo(stone11,struct1).
belongsTo(stone12,struct0).
belongsTo(stone13,struct0).
belongsTo(stone14,struct0).
belongsTo(stone15,struct0).
belongsTo(stone16,struct0).
belongsTo(stone17,struct0).
belongsTo(stone8,struct0).
belongsTo(wood0,struct0).
belongsTo(wood1,struct0).
belongsTo(wood2,struct0).
belongsTo(wood20,struct0).
belongsTo(wood21,struct0).
belongsTo(wood22,struct0).
belongsTo(wood23,struct0).
belongsTo(wood24,struct0).
belongsTo(wood25,struct0).
belongsTo(wood3,struct0).
belongsTo(wood4,struct0).
belongsTo(wood5,struct0).
bird(bluebird0).
bird(redbird1).
bird(yellowbird2).
birdOrder(bluebird0,0).
birdOrder(redbird1,1).
birdOrder(yellowbird2,2).
canCollapse(struct0,struct1).
canCollapse(struct1,struct0).
collapsesInDirection(struct0,struct1,away).
collapsesInDirection(struct1,struct0,towards).
ground_plane(384).
hasColor(bluebird0,blue).
hasColor(redbird1,red).
hasColor(yellowbird2,yellow).
hasForm(ice10,bar).
hasForm(ice18,block).
hasForm(ice19,bar).
hasForm(ice6,block).
hasForm(ice7,block).
hasForm(ice9,bar).
hasForm(stone11,ball).
hasForm(stone12,block).
hasForm(stone13,bar).
hasForm(stone14,block).
hasForm(stone15,bar).
hasForm(stone16,bar).
hasForm(stone17,bar).
hasForm(stone8,bar).
hasForm(wood0,block).
hasForm(wood1,block).
hasForm(wood2,block).
hasForm(wood20,block).
hasForm(wood21,block).
hasForm(wood22,block).
hasForm(wood23,bar).
hasForm(wood24,bar).
hasForm(wood25,block).
hasForm(wood3,bar).
hasForm(wood4,block).
hasForm(wood5,block).
hasMaterial(ice10,ice,409,321,8,17).
hasMaterial(ice18,ice,424,338,9,18).
hasMaterial(ice19,ice,425,321,8,17).
hasMaterial(ice6,ice,390,338,9,18).
hasMaterial(ice7,ice,393,321,9,17).
hasMaterial(ice9,ice,409,338,8,18).
hasMaterial(pig0,pork,402,242,7,7).
hasMaterial(pig1,pork,402,289,6,6).
hasMaterial(pig2,pork,421,289,6,6).
hasMaterial(pig3,pork,421,243,6,6).
hasMaterial(stone11,stone,411,191,9,9).
hasMaterial(stone12,stone,410,278,9,18).
hasMaterial(stone13,stone,410,241,9,19).
hasMaterial(stone14,stone,411,223,9,18).
hasMaterial(stone15,stone,412,297,8,18).
hasMaterial(stone16,stone,412,260,8,18).
hasMaterial(stone17,stone,412,205,8,18).
hasMaterial(stone8,stone,400,315,33,5).
hasMaterial(wood0,wood,366,266,9,18).
hasMaterial(wood1,wood,367,220,9,17).
hasMaterial(wood2,wood,370,284,9,18).
hasMaterial(wood20,wood,426,275,27,26).
hasMaterial(wood21,wood,426,226,26,29).
hasMaterial(wood22,wood,450,284,9,17).
hasMaterial(wood23,wood,450,238,8,18).
hasMaterial(wood24,wood,453,219,8,18).
hasMaterial(wood25,wood,454,266,9,18).
hasMaterial(wood3,wood,371,238,8,18).
hasMaterial(wood4,wood,376,277,29,26).
hasMaterial(wood5,wood,377,231,29,24).
hasOrientation(ice10,vertical).
hasOrientation(ice18,vertical).
hasOrientation(ice19,vertical).
hasOrientation(ice6,vertical).
hasOrientation(ice7,vertical).
hasOrientation(ice9,vertical).
hasOrientation(stone11,horizontal).
hasOrientation(stone12,vertical).
hasOrientation(stone13,vertical).
hasOrientation(stone14,vertical).
hasOrientation(stone15,vertical).
hasOrientation(stone16,vertical).
hasOrientation(stone17,vertical).
hasOrientation(stone8,horizontal).
hasOrientation(wood0,vertical).
hasOrientation(wood1,vertical).
hasOrientation(wood2,vertical).
hasOrientation(wood20,horizontal).
hasOrientation(wood21,vertical).
hasOrientation(wood22,vertical).
hasOrientation(wood23,vertical).
hasOrientation(wood24,vertical).
hasOrientation(wood25,vertical).
hasOrientation(wood3,vertical).
hasOrientation(wood4,horizontal).
hasOrientation(wood5,horizontal).
hasSize(bluebird0,medium).
hasSize(hill0,small).
hasSize(hill1,big).
hasSize(hill2,big).
hasSize(hill3,big).
hasSize(hill4,big).
hasSize(hill5,big).
hasSize(hill6,big).
hasSize(hill7,big).
hasSize(ice10,big).
hasSize(ice18,medium).
hasSize(ice19,big).
hasSize(ice6,medium).
hasSize(ice7,medium).
hasSize(ice9,big).
hasSize(pig0,medium).
hasSize(pig1,medium).
hasSize(pig2,medium).
hasSize(pig3,medium).
hasSize(redbird1,medium).
hasSize(stone11,medium).
hasSize(stone12,medium).
hasSize(stone13,big).
hasSize(stone14,medium).
hasSize(stone15,big).
hasSize(stone16,big).
hasSize(stone17,big).
hasSize(stone8,big).
hasSize(wood0,medium).
hasSize(wood1,medium).
hasSize(wood2,medium).
hasSize(wood20,medium).
hasSize(wood21,medium).
hasSize(wood22,medium).
hasSize(wood23,big).
hasSize(wood24,big).
hasSize(wood25,medium).
hasSize(wood3,big).
hasSize(wood4,medium).
hasSize(wood5,medium).
hasSize(yellowbird2,medium).
hill(hill0,0,357,0,3).
hill(hill1,283,356,356,3).
hill(hill2,346,302,62,10).
hill(hill3,346,256,62,11).
hill(hill4,346,209,62,10).
hill(hill5,421,302,62,10).
hill(hill6,421,256,62,11).
hill(hill7,421,209,62,10).
isAnchorPointFor(stone11,struct1).
isAnchorPointFor(wood0,struct0).
isBelow(ice10,stone8).
isBelow(ice18,ice19).
isBelow(ice19,stone8).
isBelow(ice6,ice7).
isBelow(ice7,stone8).
isBelow(ice9,ice10).
isBelow(stone12,stone16).
isBelow(stone13,stone14).
isBelow(stone14,stone17).
isBelow(stone15,stone12).
isBelow(stone16,stone13).
isBelow(stone8,hill2).
isBelow(stone8,hill5).
isBelow(stone8,stone15).
isBelow(wood0,hill3).
isBelow(wood1,hill4).
isBelow(wood2,wood0).
isBelow(wood22,wood20).
isBelow(wood22,wood25).
isBelow(wood23,wood21).
isBelow(wood23,wood24).
isBelow(wood24,hill7).
isBelow(wood25,hill6).
isBelow(wood3,wood1).
isBelow(wood3,wood5).
isCollapsable(struct0).
isCollapsable(struct1).
isLeft(pig0,stone13).
isLeft(pig1,stone12).
isLeft(stone12,pig2).
isLeft(stone13,hill6).
isLeft(stone13,pig3).
isLeft(stone15,hill5).
isLeft(stone16,hill6).
isLeft(stone17,hill7).
isLeft(wood0,wood4).
isLeft(wood1,wood5).
isLeft(wood20,wood22).
isLeft(wood20,wood25).
isLeft(wood21,wood24).
isLeft(wood4,pig1).
isLeft(wood5,pig0).
isLeft(wood5,stone13).
isOn(ice10,ice9).
isOn(ice18,hill1).
isOn(ice19,ice18).
isOn(ice6,hill1).
isOn(ice7,ice6).
isOn(ice9,hill1).
isOn(pig0,wood5).
isOn(pig1,wood4).
isOn(pig2,wood20).
isOn(pig3,wood21).
isOn(stone11,ground).
isOn(stone12,stone15).
isOn(stone13,stone16).
isOn(stone14,stone13).
isOn(stone15,stone8).
isOn(stone16,stone12).
isOn(stone17,stone14).
isOn(stone8,ice10).
isOn(stone8,ice19).
isOn(stone8,ice7).
isOn(wood0,wood2).
isOn(wood1,wood3).
isOn(wood2,hill2).
isOn(wood2,wood4).
isOn(wood20,hill5).
isOn(wood21,hill6).
isOn(wood21,wood23).
isOn(wood22,hill5).
isOn(wood23,hill6).
isOn(wood24,wood23).
isOn(wood25,wood22).
isOn(wood3,hill3).
isOn(wood4,hill2).
isOn(wood5,hill3).
isOn(wood5,wood3).
isOver(hill3,pig1).
isOver(hill4,pig0).
isOver(hill4,pig1).
isOver(hill6,pig2).
isOver(hill7,pig2).
isOver(hill7,pig3).
isOver(pig0,pig1).
isOver(pig3,pig2).
isOver(stone11,pig0).
isOver(stone11,pig1).
isOver(stone11,pig2).
isOver(stone11,pig3).
isOver(stone12,pig1).
isOver(stone12,pig2).
isOver(stone13,pig0).
isOver(stone13,pig1).
isOver(stone13,pig2).
isOver(stone13,pig3).
isOver(stone14,pig0).
isOver(stone14,pig1).
isOver(stone14,pig2).
isOver(stone14,pig3).
isOver(stone16,pig1).
isOver(stone16,pig2).
isOver(stone17,pig0).
isOver(stone17,pig1).
isOver(stone17,pig2).
isOver(stone17,pig3).
isOver(wood20,pig2).
isOver(wood21,pig2).
isOver(wood21,pig3).
isOver(wood4,pig1).
isOver(wood5,pig0).
isOver(wood5,pig1).
isRight(pig2,stone12).
isRight(pig3,stone13).
isRight(stone12,pig1).
isRight(stone13,hill3).
isRight(stone13,pig0).
isRight(stone13,wood5).
isRight(stone15,hill2).
isRight(stone16,hill3).
isRight(stone17,hill4).
isRight(wood20,pig2).
isRight(wood21,pig3).
isRight(wood24,wood21).
isRight(wood25,wood20).
isRight(wood4,wood0).
isRight(wood4,wood2).
isRight(wood5,wood1).
isTower(struct0).
pig(pig0,402,242,7,7).
pig(pig1,402,289,6,6).
pig(pig2,421,289,6,6).
pig(pig3,421,243,6,6).
protects(struct0,pig0).
protects(struct0,pig1).
protects(struct0,pig2).
protects(struct0,pig3).
protects(struct1,pig0).
protects(struct1,pig1).
protects(struct1,pig2).
protects(struct1,pig3).
scene_scale(54,1.005).
shape(bluebird0,ball, 125,321.5,23,[2.75]).
shape(hill0,poly, 141,358,0,[2,[0,357],[0,360]]).
shape(hill1,poly, 461,358,1068,[6,[283,358],[284,359],[639,358],[639,356],[349,358],[324,356]]).
shape(hill2,poly, 377,307,620,[4,[346,303],[347,312],[408,311],[407,302]]).
shape(hill3,poly, 377,262,682,[5,[346,257],[346,264],[370,267],[408,265],[407,256]]).
shape(hill4,poly, 377,214,620,[4,[346,210],[347,219],[408,218],[407,209]]).
shape(hill5,poly, 452,307,620,[5,[421,303],[421,310],[440,312],[483,311],[482,302]]).
shape(hill6,poly, 452,262,682,[5,[421,257],[421,264],[459,267],[483,265],[482,256]]).
shape(hill7,poly, 452,214,620,[4,[421,210],[422,219],[483,218],[482,209]]).
shape(ice10,rect, 413,329.5,136,[8,17,1.57079633]).
shape(ice18,rect, 428.5,347,162,[9,18,1.57079633]).
shape(ice19,rect, 429,329.5,136,[8,17,1.57079633]).
shape(ice6,rect, 394.5,347,162,[9,18,1.57079633]).
shape(ice7,rect, 397.5,329.5,153,[9,17,1.57079633]).
shape(ice9,rect, 413,347,144,[8,18,1.57079633]).
shape(pig0,ball, 405,245.5,70,[4.75]).
shape(pig1,ball, 405.5,292.5,63,[4.5]).
shape(pig2,ball, 423.5,291.5,50,[4]).
shape(pig3,ball, 424,246.5,56,[4.25]).
shape(redbird1,ball, 109,351,50,[4]).
shape(stone11,ball, 416,196.5,153,[7]).
shape(stone12,rect, 414.5,287,162,[9,18,1.57079633]).
shape(stone13,rect, 414.5,250.5,171,[9,19,1.57079633]).
shape(stone14,rect, 415.5,232,162,[9,18,1.57079633]).
shape(stone15,rect, 416,306,144,[8,18,1.57079633]).
shape(stone16,rect, 416,269,144,[8,18,1.57079633]).
shape(stone17,rect, 416,214,144,[8,18,1.57079633]).
shape(stone8,rect, 416.5,317.5,165,[5,33,0]).
shape(wood0,rect, 370.5,275,162,[9,18,1.57079633]).
shape(wood1,rect, 371.5,228.5,153,[9,17,1.57079633]).
shape(wood2,rect, 374.5,293,162,[9,18,1.57079633]).
shape(wood20,rect, 439.5,288,120,[3.53553391,33.9411255,2.35619449]).
shape(wood21,rect, 439,240.5,121,[3.48415774,34.80185992,2.29336264]).
shape(wood22,rect, 454.5,292.5,153,[9,17,1.57079633]).
shape(wood23,rect, 454,247,144,[8,18,1.57079633]).
shape(wood24,rect, 457,228,144,[8,18,1.57079633]).
shape(wood25,rect, 458.5,275,162,[9,18,1.57079633]).
shape(wood3,rect, 375,247,144,[8,18,1.57079633]).
shape(wood4,rect, 390.5,290,138,[3.97863334,34.82718533,0.69115038]).
shape(wood5,rect, 391.5,243,122,[3.58638771,34.18976134,0.69115038]).
shape(yellowbird2,ball, 83,350.5,70,[4.75]).
situation_name('Level1-12').
slingshotPivot(125.72,320.48).
structure(struct0).
structure(struct1).

belongsTo(ice10,struct1).
belongsTo(ice12,struct1).
belongsTo(pig0,struct0).
belongsTo(pig1,struct0).
belongsTo(pig2,struct3).
belongsTo(stone11,struct2).
belongsTo(stone2,struct1).
belongsTo(stone3,struct0).
belongsTo(stone4,struct1).
belongsTo(stone5,struct1).
belongsTo(stone7,struct1).
belongsTo(stone8,struct1).
belongsTo(wood0,struct0).
belongsTo(wood1,struct0).
belongsTo(wood6,struct0).
belongsTo(wood9,struct1).
bird(blackbird2).
bird(redbird1).
bird(yellowbird0).
birdOrder(blackbird2,2).
birdOrder(redbird1,1).
birdOrder(yellowbird0,0).
canCollapse(struct0,struct1).
canCollapse(struct1,struct0).
canCollapse(struct2,struct1).
canCollapse(struct2,struct3).
canCollapse(struct3,struct2).
collapsesInDirection(struct0,struct1,away).
collapsesInDirection(struct1,struct0,towards).
collapsesInDirection(struct1,struct2,away).
collapsesInDirection(struct2,struct1,towards).
collapsesInDirection(struct2,struct3,away).
collapsesInDirection(struct3,struct2,towards).
ground_plane(384).
hasColor(blackbird2,black).
hasColor(redbird1,red).
hasColor(yellowbird0,yellow).
hasForm(ice10,block).
hasForm(ice12,cube).
hasForm(stone11,ball).
hasForm(stone2,block).
hasForm(stone3,ball).
hasForm(stone4,block).
hasForm(stone5,block).
hasForm(stone7,cube).
hasForm(stone8,cube).
hasForm(wood0,cube).
hasForm(wood1,bar).
hasForm(wood6,cube).
hasForm(wood9,bar).
hasMaterial(ice10,ice,308,326,7,8).
hasMaterial(ice12,ice,335,326,8,8).
hasMaterial(pig0,pork,267,235,6,6).
hasMaterial(pig1,pork,276,235,6,6).
hasMaterial(pig2,pork,351,343,11,11).
hasMaterial(stone11,stone,336,307,9,9).
hasMaterial(stone2,stone,267,312,23,14).
hasMaterial(stone3,stone,269,245,11,11).
hasMaterial(stone4,stone,280,325,8,9).
hasMaterial(stone5,stone,288,321,13,10).
hasMaterial(stone7,stone,292,315,7,7).
hasMaterial(stone8,stone,300,312,9,9).
hasMaterial(wood0,wood,249,263,11,11).
hasMaterial(wood1,wood,258,260,35,4).
hasMaterial(wood6,wood,290,263,11,11).
hasMaterial(wood9,wood,302,321,42,4).
hasOrientation(ice10,vertical).
hasOrientation(ice12,horizontal).
hasOrientation(stone11,horizontal).
hasOrientation(stone2,horizontal).
hasOrientation(stone3,horizontal).
hasOrientation(stone4,vertical).
hasOrientation(stone5,horizontal).
hasOrientation(stone7,horizontal).
hasOrientation(stone8,horizontal).
hasOrientation(wood0,horizontal).
hasOrientation(wood1,horizontal).
hasOrientation(wood6,horizontal).
hasOrientation(wood9,horizontal).
hasSize(blackbird2,medium).
hasSize(hill0,big).
hasSize(hill1,medium).
hasSize(hill2,medium).
hasSize(hill3,small).
hasSize(hill4,big).
hasSize(hill5,small).
hasSize(hill6,medium).
hasSize(hill7,small).
hasSize(hill8,small).
hasSize(ice10,medium).
hasSize(ice12,small).
hasSize(pig0,medium).
hasSize(pig1,medium).
hasSize(pig2,medium).
hasSize(redbird1,medium).
hasSize(stone11,medium).
hasSize(stone2,medium).
hasSize(stone3,medium).
hasSize(stone4,medium).
hasSize(stone5,medium).
hasSize(stone7,small).
hasSize(stone8,small).
hasSize(wood0,small).
hasSize(wood1,medium).
hasSize(wood6,small).
hasSize(wood9,medium).
hasSize(yellowbird0,medium).
hill(hill0,0,353,117,7).
hill(hill1,236,273,23,8).
hill(hill2,257,334,50,9).
hill(hill3,257,236,9,22).
hill(hill4,283,334,356,25).
hill(hill5,284,236,9,22).
hill(hill6,291,273,23,8).
hill(hill7,307,334,9,23).
hill(hill8,368,334,9,22).
isAnchorPointFor(pig2,struct3).
isAnchorPointFor(stone11,struct2).
isAnchorPointFor(stone2,struct1).
isAnchorPointFor(wood0,struct0).
isBelow(ice10,wood9).
isBelow(ice12,wood9).
isBelow(stone3,pig0).
isBelow(stone3,pig1).
isBelow(stone4,stone2).
isBelow(stone5,stone2).
isBelow(stone5,stone7).
isBelow(stone5,stone8).
isBelow(wood0,wood1).
isBelow(wood1,hill3).
isBelow(wood1,hill5).
isBelow(wood1,stone3).
isBelow(wood6,wood1).
isBelow(wood9,stone8).
isCollapsable(struct0).
isCollapsable(struct1).
isCollapsable(struct2).
isCollapsable(struct3).
isLeft(pig0,pig1).
isLeft(pig1,hill5).
isLeft(stone2,stone7).
isLeft(stone3,hill5).
isLeft(stone4,stone5).
isLeft(stone5,wood9).
isLeft(stone7,stone8).
isLeft(stone7,wood9).
isOn(ice10,hill4).
isOn(ice10,hill7).
isOn(ice12,hill4).
isOn(pig0,stone3).
isOn(pig1,stone3).
isOn(pig2,hill4).
isOn(stone11,ground).
isOn(stone2,stone4).
isOn(stone2,stone5).
isOn(stone3,wood1).
isOn(stone4,hill2).
isOn(stone4,hill4).
isOn(stone5,hill2).
isOn(stone5,hill4).
isOn(stone7,stone5).
isOn(stone8,stone5).
isOn(stone8,wood9).
isOn(wood0,hill1).
isOn(wood1,wood0).
isOn(wood1,wood6).
isOn(wood6,hill6).
isOn(wood9,ice10).
isOn(wood9,ice12).
isOver(hill3,stone3).
isOver(hill4,pig2).
isOver(hill5,stone3).
isOver(pig0,stone3).
isOver(pig1,stone3).
isRight(pig0,hill3).
isRight(pig1,pig0).
isRight(stone3,hill3).
isRight(stone5,stone4).
isRight(stone7,stone2).
isRight(stone8,stone7).
isRight(wood9,stone5).
isRight(wood9,stone7).
pig(pig0,267,235,6,6).
pig(pig1,276,235,6,6).
pig(pig2,351,343,11,11).
scene_scale(50,1.005).
shape(blackbird2,ball, 65,348.5,188,[7.75]).
shape(hill0,poly, 141,357,819,[5,[0,357],[0,360],[117,356],[115,353],[113,356]]).
shape(hill1,poly, 248,278,184,[4,[236,274],[236,280],[259,281],[258,273]]).
shape(hill2,poly, 282,339,450,[4,[257,335],[257,342],[307,343],[306,334]]).
shape(hill3,poly, 262,248,198,[4,[257,237],[257,257],[266,258],[264,236]]).
shape(hill4,poly, 461,347,8900,[8,[283,358],[284,359],[639,358],[639,356],[343,356],[342,334],[334,335],[333,356]]).
shape(hill5,poly, 289,248,198,[4,[284,237],[284,257],[293,258],[292,236]]).
shape(hill6,poly, 303,278,184,[4,[291,274],[291,280],[314,281],[313,273]]).
shape(hill7,poly, 312,346,207,[4,[307,335],[308,357],[316,356],[315,334]]).
shape(hill8,poly, 373,346,198,[4,[368,335],[368,355],[377,356],[376,334]]).
shape(ice10,rect, 311.5,330,56,[7,8,1.57079633]).
shape(ice12,rect, 339,330,64,[8,8,0]).
shape(pig0,ball, 270.5,238.5,63,[4.5]).
shape(pig1,ball, 279.5,238.5,63,[4.5]).
shape(pig2,ball, 357,349,201,[8]).
shape(redbird1,ball, 96.5,351,56,[4.25]).
shape(stone11,ball, 341,312.5,153,[7]).
shape(stone2,rect, 278.5,319,95,[4.17078687,22.79705151,0.4712389]).
shape(stone3,ball, 275.5,251.5,201,[8]).
shape(stone4,rect, 284,329.5,72,[8,9,1.57079633]).
shape(stone5,rect, 294.5,326,56,[4.48898157,12.52501476,0.4712389]).
shape(stone7,rect, 295.5,318.5,49,[7,7,0]).
shape(stone8,rect, 304.5,316.5,40,[6.36396103,6.36396103,2.35619449]).
shape(wood0,rect, 254.5,268.5,78,[8.78325086,8.8980503,1.79070781]).
shape(wood1,rect, 275.5,262,140,[4,35,0]).
shape(wood6,rect, 295.5,268.5,80,[8.74342484,9.21462822,0.25132741]).
shape(wood9,rect, 323,323,168,[4,42,0]).
shape(yellowbird0,ball, 113,322.5,44,[3.75]).
situation_name('Level1-8').
slingshotPivot(112.72,323.48).
structure(struct0).
structure(struct1).
structure(struct2).
structure(struct3).

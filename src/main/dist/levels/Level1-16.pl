belongsTo(pig0,struct0).
belongsTo(pig1,struct4).
belongsTo(stone0,struct1).
belongsTo(stone1,struct3).
belongsTo(tnt0,struct2).
bird(redbird0).
bird(redbird1).
bird(redbird2).
birdOrder(redbird0,0).
birdOrder(redbird1,1).
birdOrder(redbird2,2).
canCollapse(struct1,struct0).
canCollapse(struct1,struct2).
canCollapse(struct2,struct1).
canCollapse(struct2,struct3).
canCollapse(struct3,struct2).
canCollapse(struct3,struct4).
canExplode(tnt0,stone0).
canExplode(tnt0,stone1).
canExplode(tnt0,tnt0).
collapsesInDirection(struct0,struct1,away).
collapsesInDirection(struct1,struct0,towards).
collapsesInDirection(struct1,struct2,away).
collapsesInDirection(struct2,struct1,towards).
collapsesInDirection(struct2,struct3,away).
collapsesInDirection(struct3,struct2,towards).
collapsesInDirection(struct3,struct4,away).
collapsesInDirection(struct4,struct3,towards).
ground_plane(384).
hasColor(redbird0,red).
hasColor(redbird1,red).
hasColor(redbird2,red).
hasForm(stone0,ball).
hasForm(stone1,ball).
hasMaterial(pig0,pork,334,333,6,6).
hasMaterial(pig1,pork,444,333,6,6).
hasMaterial(stone0,stone,367,278,11,11).
hasMaterial(stone1,stone,407,278,11,11).
hasMaterial(tnt0,tnt,384,272,15,15).
hasOrientation(stone0,horizontal).
hasOrientation(stone1,horizontal).
hasSize(hill0,small).
hasSize(hill1,big).
hasSize(pig0,medium).
hasSize(pig1,medium).
hasSize(redbird0,medium).
hasSize(redbird1,medium).
hasSize(redbird2,medium).
hasSize(stone0,medium).
hasSize(stone1,medium).
hasSize(tnt0,big).
hill(hill0,0,357,0,3).
hill(hill1,283,285,356,74).
isAnchorPointFor(pig0,struct0).
isAnchorPointFor(pig1,struct4).
isAnchorPointFor(stone0,struct1).
isAnchorPointFor(stone1,struct3).
isAnchorPointFor(tnt0,struct2).
isCollapsable(struct0).
isCollapsable(struct1).
isCollapsable(struct2).
isCollapsable(struct3).
isCollapsable(struct4).
isOn(pig0,hill1).
isOn(pig1,hill1).
isOn(stone0,hill1).
isOn(stone1,hill1).
isOn(tnt0,hill1).
isOver(hill1,pig0).
isOver(hill1,pig1).
pig(pig0,334,333,6,6).
pig(pig1,444,333,6,6).
scene_scale(54,1.005).
shape(hill0,poly, 141,358,0,[2,[0,357],[0,360]]).
shape(hill1,poly, 461,323,26344,[20,[283,358],[284,359],[639,358],[639,356],[465,357],[449,340],[446,342],[408,302],[415,301],[414,292],[403,291],[402,285],[381,286],[380,292],[371,293],[372,302],[377,303],[338,342],[335,340],[319,357]]).
shape(pig0,ball, 337.5,336,56,[4.25]).
shape(pig1,ball, 447,336,63,[4.5]).
shape(redbird0,ball, 116.5,314,44,[3.75]).
shape(redbird1,ball, 100,351,50,[4]).
shape(redbird2,ball, 83,351,50,[4]).
shape(stone0,ball, 373,284.5,201,[8]).
shape(stone1,ball, 413.5,284.5,201,[8]).
shape(tnt0,rect, 391.5,279.5,225,[15,15,0]).
situation_name('Level1-16').
slingshotPivot(116.72,316.48).
structure(struct0).
structure(struct1).
structure(struct2).
structure(struct3).
structure(struct4).

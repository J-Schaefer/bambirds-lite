# BamBirds lite

## Prerequisities

- Some JRE/JDK >= 8, <= 17 (Application was compiled with JDK 8, for ScienceBirds JRE >= 12 is required)
- SWI-Prolog >= 8.2

## Customizing prolog

The prolog files are all in `lib/planner`, add your custom plans `lib/planner/plans.pl`. There are already two example plans in that file, including an explanation on how they work.

You can use many utilites for physics, geometric algorithms or for shots to make your plans more advanced. All the available predicates from the data file are found in `lib/planner/data.pl`

## Game

You can download the game for you platform from the [science-birds-novelty repository](https://gitlab.com/aibirds/sciencebirdsframework/-/tree/master/ScienceBirds)

### Standalone

Starting the game itself can be done by executing the file `9001.x86_64`

To add our prepared levels copy the `xml` files in the `levels` folder here into `9001_Data/StreaminAssets/Levels` and replace the `config.xml` in the game folder with the file `conf/config.xml`. Then start the game from the command line with the flag `--configpath config.xml`

### As Game-Server

To start the game server, in order to connect with it using the agent, you only need to run this command in the game folder:

```
java -jar game_playing_interface.jar
```

## Execution

### Only prolog

To start the planner immediately you can run
```bash
$ swipl start.pl
```
and then enter the path to your situation file (ex: `"situation1-1".`).

For a more specific use case you can only load the main file:
```shell
$ swipl main.pl
```
```prolog
% Load data without prompt
1 ?- load_data("path/to/situation/file").
% Run the planner
2 ?- initiate_planner(AllPlans). 
% Run planner and then export the plans with latex
3 ?- initiate_planner(AllPlans), export_tikz(AllPlans).

% or execute any custom predicates / functions
4 ?- pig(P), isHittable(P, Angle), shot_at_target(P,ImpactAngle,Shot).
```

Because of the module structure you will sometimes see something like this:
```
Correct to: "tikz:export_tikz(AllPlans)"?
```
Just enter `y` and everything will be fine

### Full agent

> You will need to start the SBServer first

To run the agent, run the command 
```bash
./bin/bambirds -h <hostname> -t <teamid> -s <pathtoswipl> --science-birds
```

* \<hostname\> - name of the server (default `localhost`)
* \<teamid\> - ID of the BamBirds team (default `1`)
* \<pathtoswipl\> - path to SWI-Prolog (swipl) in your machine (default `swipl` from `PATH`)
* `--science-birds` - Specify that the server is using ScienceBirds (not necessary since it is the default)

Run
```bash
./bin/bambirds --help 
```
for more options.

# BamBirds Lite

- [BamBirds Lite](#bambirds-lite)
  - [Repository structure](#repository-structure)
  - [Prerequisities](#prerequisities)
    - [Using AngryBirds (via Chromium)](#using-angrybirds-via-chromium)
    - [Using ScienceBirds](#using-sciencebirds)
  - [Run from distribution](#run-from-distribution)
  - [Run from source](#run-from-source)
    - [With ScienceBirds](#with-sciencebirds)
    - [Using Gradle](#using-gradle)
    - [Configuration](#configuration)
    - [Troubleshooting](#troubleshooting)
    - [Debugging](#debugging)

## Repository structure

BamBirds is a gradle project and contains the following modules:

- [common/](common/): Common Utils and Data Structures
- [client/](client/): Client for connecting to ABServer or SBServer
- [vision/](vision/): Vision recognition for AngryBirds and ScienceBirds
- [planner/](planner/): Action planner (More info in [the README](planner/README.md))
  - [src/main/prolog](planner/src/main/prolog/): Prolog Planner
  - [src/main/java](planner/src/main/java/): Knowledge generation and plan parsing for Prolog
- [level_selection/](level_selection/): Module for the Level Selection, containing Models for Prediction of Score and Level Outcome
  - [src/main/java](level_selection/src/main/java): The module for executing models in the agent
- [src/](src/): Bambirds Agent
  - [main/java](src/main/java): Java Code for the agent

Additional Resources and Information can be found here:

- [doc/](doc/): documentation, project reports etc.

## Prerequisities

- Install Java >= 8, <= 17 (Optimally >= 12 so ScienceBirds can be run with the same version)
- [Install SWI-Prolog >= 8.2](https://www.swi-prolog.org/Download.html)
  - Make sure to set `PATH_TO_SWIPL` in [`config.properties`](#configuration) to the executable on the system if is not located on the `PATH` (System Variables on Windows)

Or if you want to run with docker:

- docker
- docker-compose

### Using AngryBirds (via Chromium)

- This option is not included here, since the setup is much simpler in ScienceBirds

### Using ScienceBirds

- Clone the [ScienceBirds Framework](https://gitlab.com/aibirds/sciencebirdsframework)
  - Install a Java 12 or higher
  - Extract the ScienceBirds Game in [ScienceBirds folder](https://gitlab.com/aibirds/sciencebirdsframework/-/tree/master/ScienceBirds)

## Run from distribution

Get the latest distribution from the [releases](https://sme.uni-bamberg.de/bambirds/bambirds-lite/-/releases)

The distribution contains a `bin` and a `lib` folder.

> For running the game see [Run from source](#run-from-source)

To run the agent, run the command

```bash
./bin/bambirds
```

Configuration can be done either with the `config.properties` file or with commandline options.

For more information run

```bash
./bin/bambirds --help
```

## Run from source

Make sure you are in the root directory of the BamBird repository (the same
directory this very file is located in).
### With ScienceBirds

1. Start the ScienceBirds Unity Game

2. Run the AIBirds server:

   Open the folder sciencebirdsframework and use java 12 or higher to run. For example:

   ```bash
   /usr/lib/jvm/java-13-openjdk/bin/java -jar game_playing_interface.jar
   ```

   This will start the server application.

3. Run the Agent:

   Open another terminal and enter this command:

   ```bash
   ./gradlew :run
   ```

   > Make sure to set `SERVER_TYPE=SCIENCE_BIRDS` in `config.properties`

4. Shutdown:

   **Server**: open the terminal in which you started the server and press `Ctrl-C`.

   **Agent**: open the terminal in which you started the agent and press `Ctrl-C`.

### Using Gradle

The BamBird agent can be compiled, run and packaged
using [Gradle](https://gradle.org/).

Use one of the following tasks:

- **`tasks`**

  Display all available Tasks

- **`game:abserver:run`**

  Execute the AIBirds server (`game/abserver`).

- **`:run`**

  Execute the BamBird agent (`de.uniba.sme.bambirds.BamBirds`).

  [Configuration](#configuration) should be done via the `config.properties` file.

  For testing purposes the default values should be just fine.

  > The `:` before `run` is required to only run the root project and not other projects with the application plugin

- **`test`**

  Run JUnit tests

- **`distTar`/`distZip`**

  Create a tar/zip containing executables, all dependencies and Prolog files in the `build/distributions/` directory. Files in [src/main/dist](src/main/dist) will be included.

- `compileJava`

  Compile all Java files

- `clean`

  Remove the `build/` directory.

- `javadoc`

  Generate the javadoc in `build/docs/javadoc` directory

- `<subproject>:<task>`
  Execute a Task for a subproject

### Configuration

All configuration is saved in `config.properties` (file is created on the first execution of the agent):

| Name                                    |             Options/Type              | Use                                                                                                                                                                                                                             | Access in source                                 |
| :-------------------------------------- | :-----------------------------------: | :------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ | :----------------------------------------------- |
| `HOST`                                  |        any server domain or ip        | the location of ABServer or SBServer                                                                                                                                                                                            | `Settings.SERVER_HOST`                           |
| `GAME_MODE`                             | `COMPETITION` \| `TRAINING` \| `DEMO` | can be used for changing mode of Agent between competition or training. The demo mode enables the visual export of the generated plans and display in Java (only supported on Unix Systems, requires `pdflatex` and `convert`). | `Settings.GAME_MODE`                             |
| `ROUNDS`                                |              `int` value              | when level-selection disabled the number of complete rounds to play, `-1` for unlimited                                                                                                                                         | `Settings.ROUNDS`                                |
| `TEAM_ID`                               |             `uint` value              | The id used to connect to server                                                                                                                                                                                                | `Settings.TEAM_ID`                               |
| `SERVER_TYPE`                           |   `ANGRY_BIRDS` \| `SCIENCE_BIRDS`    | Switch between the ABServer and SBServer (they have slightly different APIs)                                                                                                                                                    | `Settings.SERVER_TYPE`                           |
| `START_LEVEL`                           |              `int` value              | The id of the first level to play                                                                                                                                                                                               | `Settings.START_LEVEL`                           |
| `PATH_TO_SWIPL`                         |         file path or command          | The location of the swipl executable (On windows for example `"C:\\Program Files\\swipl\\bin\\swipl"`)                                                                                                                          | `Settings.PATH_TO_SWIPL`                         |
| `LEVEL_RANGE`                           |              `int` value              | The range of levels to play, 0 for all available, 1 for only the `START_LEVEL`                                                                                                                                                  | `Settings.START_LEVEL`                           |
| `VISUAL_DEBUG_ENABLED`                  |            `boolean` value            | Enable Visual Debugging                                                                                                                                                                                                         | `Settings.VISUAL_DEBUG_ENABLED`                  |
| `EXPORT_LEVEL_STATS`                    |            `boolean` value            | Export level statistics                                                                                                                                                                                                         | `Settings.EXPORT_LEVEL_STATS`                    |
| `DISABLE_LEVEL_SELECTION`               |            `boolean` value            | Disable the level seletction completely and run levels iterative                                                                                                                                                                | `Settings.DISABLE_LEVEL_SELECTION`               |
| `LEVEL_SELECTION_FIRST_ROUND_ITERATIVE` |            `boolean` value            | Run the first round of levels iterative                                                                                                                                                                                         | `Settings.LEVEL_SELECTION_FIRST_ROUND_ITERATIVE` |

Optionally also cli arguments can be passed to gradle with

```bash
./gradlew run --args='--help'
```

### Troubleshooting

- Verify that Gradle picks the correct Java version by running `./gradlew --version`.
- Verify that you have the correct [Chromium version](http://aibirds.org/basic-game-playing-software/offline-chrome.html)
- Verify that you can execute swi-prolog from the command line: `swipl` (or the path you have set in `config.properties`)

### Debugging

The Debugging can be done also via gradle with an external debugger like in eclipse, intellij or vscode (with the "Debugger for Java" extension)

Run the jvm in debugging mode with

```bash
./gradlew :run --debug-jvm
```

and then attach to it on Port 5005. For VSCode the debugging task "Debug (Attach) - Local" will do exactly that.
